//
//  PhyloAboutInfoViewController.h
//  Phylo
//
//  Created by Michael Riyanto on 2013-07-26.
//  Copyright (c) 2013 Cody. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface PhyloAboutInfoViewController : UIViewController{
    IBOutlet UIScrollView *scroller;

    
}

@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)back:(id)sender;

@end
