//
//  PhyloCardTest.m
//  Phylo
//
//  Created by Jacky Chao on 7/3/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCard.h"
#import "PhyloCardTest.h"



@implementation PhyloCardTest

- (void)testID
{
    PhyloCard* newCard = [[PhyloCard alloc] init:@"Evening Grosbeak" withWebPath:@"http://phylogame.org/2013/04/30/evening-grosbeak/" withCardID:123 withImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://farm7.staticflickr.com/6232/6217183371_e05d52da40_o_d.jpg"]]]];
    
    STAssertEquals(newCard.cardIdentifier, 123, @"Should be equal to 123");
}

-(void)testWebPath
{
    PhyloCard* newCard = [[PhyloCard alloc] init:@"Evening Grosbeak" withWebPath:@"http://phylogame.org/2013/04/30/evening-grosbeak/" withCardID:123 withImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://farm7.staticflickr.com/6232/6217183371_e05d52da40_o_d.jpg"]]]];

        STAssertEqualObjects(newCard.webPath, @"http://phylogame.org/2013/04/30/evening-grosbeak/", @"Webpaths should be equal");
    
}

-(void)testName
{
    PhyloCard* newCard = [[PhyloCard alloc] init:@"Evening Grosbeak" withWebPath:@"http://phylogame.org/2013/04/30/evening-grosbeak/" withCardID:123 withImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://farm7.staticflickr.com/6232/6217183371_e05d52da40_o_d.jpg"]]]];
    STAssertEquals(newCard.cardName, @"Evening Grosbeak", @"Names should be equal");
}

@end
	