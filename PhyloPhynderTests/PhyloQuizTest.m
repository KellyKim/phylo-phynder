//
//  PhyloQuizTest.m
//  Phylo
//
//  Created by Jacky Chao on 7/3/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloQuiz.h"
#import "PhyloQuizTest.h"

@implementation PhyloQuizTest

- (void) testMakeQuiz{
    PhyloQuiz* newQuiz = [[PhyloQuiz alloc] init:@"Is CMPT275 fun!?" withAnswers:[[NSArray alloc] initWithObjects:@"Fo' sure", @"Hell no!", @"Not bad", @"I think imma fail", nil] awardingpoints:100 correctAnswerID:1 withImage:nil];
    
    
    
    STAssertEquals(newQuiz.question, @"Is CMPT275 fun!?", @"Questions should be the same");
    STAssertEqualObjects([newQuiz.answerSet 	objectAtIndex:0], @"Fo' sure", @"Set should be fine");
    
    
}

- (void) testMakeRandomizedQuiz{
    PhyloQuiz* newQuiz = [[PhyloQuiz alloc] init:@"Is CMPT275 fun!?" withAnswers:[[NSArray alloc] initWithObjects:@"Fo' sure", @"Hell no!", @"Not bad", @"I think imma fail", nil] awardingpoints:100 correctAnswerID:2 withImage:nil];
    NSMutableArray* randomizedSet = [[NSMutableArray alloc]init];
    randomizedSet = [newQuiz randomizeAnswerSet];
    NSLog(@"%@,%@,%@,%@",[randomizedSet objectAtIndex:0],[randomizedSet objectAtIndex:1],[randomizedSet objectAtIndex:2], [randomizedSet objectAtIndex:3]);
}

@end
