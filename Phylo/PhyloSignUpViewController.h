//
//  PhyloSignUpViewController.h
//  Phylo
//
//  Created by Yun Zeng on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloSignUpViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *UserNameField;
@property (strong, nonatomic) IBOutlet UITextField *PasswordField;
@property (strong, nonatomic) IBOutlet UITextField *Re_PasswordField;
- (IBAction)SignUp:(id)sender;
- (IBAction)Cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *PubOrPriLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *PubOrPri;


@end
