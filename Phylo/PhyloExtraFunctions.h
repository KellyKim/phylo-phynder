//
//  PhyloExtraFunctions.h
//  Phylo
/*
    This class implements any extra methods that will be used by other classes. Thus, it will contain static methods for general purposes. At the point of version 3, the only method implmented is the playSound method.
 */
//  Created by Jacky Chao on 2013-07-27.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloExtraFunctions : NSObject

+(void) playSound:(NSString*)fileName withType:(NSString*)type;

@end
