//
//  PhyloQuiz.m
//  Phylo
//
//  PhyloQuiz implementation file.
//
//
//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import "PhyloQuiz.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"

@implementation PhyloQuiz
@synthesize question,answerSet,points,correctAnswerID,displayImage, quizCompleted;

//adopting app container from delegate
- (PhyloDataContainer*) theAppDataContainer; 
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}


//quiz initialization method
-(PhyloQuiz*) init:(NSString*)quizQuestion withAnswers:(NSArray*)providedAnswerSet awardingpoints:(int)quizPoints correctAnswerID:(int)ID withImage:(UIImage*)quizImage{
    self = [super init];
    
    if (self){
        question = quizQuestion;
        answerSet = providedAnswerSet;
        points = quizPoints;
        correctAnswerID = ID;
        displayImage = quizImage;
        quizCompleted = NO;
    }
    return self;
}


/*
 The following method will be called when the user taps an answer button on PhyloQuizViewController. Result will depend on whether the answer is correct or not. This will in chain cause checkpoint class to mark visited as YES.

If answer is correct. Ex. User selected row id == correctAnswerID
 then access user's profile and add points. 
else, if not correct, minus 10 points instead.
 */
-(BOOL) quizCompletion:(NSInteger)selectedAnswerID{
    
    self.quizCompleted = YES;
    PhyloDataContainer* updateContainer = [self theAppDataContainer];
    
    if(selectedAnswerID==correctAnswerID){
        updateContainer.points=updateContainer.points+self.points;
        [self updateToDatabase];
        return YES;
        
    }
    else{
        updateContainer.points=updateContainer.points-10;
        [self updateToDatabase];
        return NO;
    }
    
    
    
}

/* For extra feature purposes, this method will be used to randomize answer set order.
 */
-(NSMutableArray*) randomizeAnswerSet{
    NSMutableArray* randomized = [[NSMutableArray alloc]initWithArray:answerSet];
    int count = [self.answerSet count];
    for (int i=0;i<count;i++){
        int nElements = count - i;
        int n =(arc4random() % nElements) + i;
        [randomized exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    return randomized;
}

// This method updates users' points on the database through a php script
-(void) updateToDatabase{
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/updateData.php?UserName=%@&newVal=%d&destination=%@", [self theAppDataContainer].userName,[self theAppDataContainer].points  ,@"Points"];
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    NSLog(@"Correct points %@",strResult);
}



@end
