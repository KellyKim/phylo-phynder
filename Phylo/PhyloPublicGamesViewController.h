//
//  PhyloPublicGamesViewController.h
//  Phylo
/*----------------------------------------------
 This viewcon provides a list of public games, which will be pulled from the database, available to the user. It also adopts searchbar methods.
 ----------------------------------------------*/
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloMapViewController.h"

@interface PhyloPublicGamesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *search;
@property (strong, nonatomic) IBOutlet UITableView *searchesTable;


-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope;



-(BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString;
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption;



@end
