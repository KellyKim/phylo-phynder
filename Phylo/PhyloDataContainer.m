//
//  PhyloDataContainer.m
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloDataContainer.h"

@implementation PhyloDataContainer
@synthesize userName;
@synthesize loggedIn;
@synthesize points;
@synthesize dateJoined;
@synthesize  tempGame,tempCard,tempQuiz,tempCheckpoints,gamesCompleted;
@synthesize cardsCollected;
@synthesize soundEnabled;
@synthesize userPassword;



@end
