//
//  PhyloExtraFunctions.m
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-27.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloExtraFunctions.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation PhyloExtraFunctions

//This method takes a filename and the corresponding file type, and plays the sound. There is no validation included at the point of version 3.
+(void) playSound:(NSString*)fileName withType:(NSString*)type{
    SystemSoundID soundID;
    NSString *soundFileName = [[NSBundle mainBundle]pathForResource:fileName ofType:type];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundFileName], &soundID);
    AudioServicesPlaySystemSound(soundID);

}

@end
