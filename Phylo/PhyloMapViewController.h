//
//  MapViewController.h
//  Phylo
//
//  This class implements the map-GUI on PhyloPhynder. It is responsible of presenting the map of the current game the user is in. It does this through coordinating the set of checkpoints (which is stored in the data container) to the map annotations. 
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <coreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "PhyloCheckpoints.h"
#import "PhyloCard.h"
#import "PhyloQuiz.h"


@interface PhyloMapViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate>{
    
    CLLocationManager* locationManager;
    
}
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) PhyloCheckpoints* tempCheckpoint;
@property (nonatomic, strong) NSMutableArray* setOfCheckpoints;
@property (nonatomic, strong) NSMutableArray* setOfAnnotations;
-(IBAction)foundIt:(id)sender;
@end