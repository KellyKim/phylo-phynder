//
//  PhyloCreateViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCreateViewController.h"

@interface PhyloCreateViewController ()

@end

@implementation PhyloCreateViewController

@synthesize nameOfScavengerHunt;
@synthesize string = _string;

@synthesize privatePublic;
@synthesize password;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //set screen title
        self.title = NSLocalizedString(@"Create a Game", @"Create a Game");
    }
    return self;
}

- (void)viewDidLoad
{
    //load this page
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) privatePublicIndexChanged {
    switch (self.privatePublic.selectedSegmentIndex) {
        case 0:
            password.hidden = YES;
            break;
        case 1:
            password.hidden = NO;
            break;
        default:
            password.hidden = YES;
            break;
    }
}

-(IBAction) nextButtonPressed:(id)sender {

    //go to select map screen when next button is touched
    PhyloSelectMapViewController* selectMapViewController = [[PhyloSelectMapViewController alloc] initWithNibName:@"PhyloSelectMapViewController" bundle:nil];
    [self.navigationController pushViewController:selectMapViewController animated:YES];
}

-(IBAction) dismissKeyboard:(id)sender{
    //[nameOfScavengerHunt resignFirstResponder];
    [sender resignFirstResponder];
    //return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.nameOfScavengerHunt)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}
@end
