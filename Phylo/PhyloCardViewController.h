//
//  PhyloCardViewController.h
//  Phylo
//
//  Created by Yun Zeng on 7/14/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCardAttributes.h"


@interface PhyloCardViewController : UIViewController

//view controler that loads the attributes
@property (weak, nonatomic) IBOutlet UIImageView *art;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIImageView *foodhierachy;
@property (weak, nonatomic) IBOutlet UIImageView *size;
@property (weak, nonatomic) IBOutlet UITextView *name;
@property (weak, nonatomic) IBOutlet UITextView *nameLatin;
@property (weak, nonatomic) IBOutlet UIWebView *cardContent;
@property (weak, nonatomic) IBOutlet UITextView *temperature;

//attributes saved in variables
@property (nonatomic, strong) NSString *permLink;
@property (nonatomic, strong) NSMutableArray *json;
@property (nonatomic, strong) PhyloCardAttributes *cardAttributes;
@property (nonatomic, strong) NSString *cardname;
@property (nonatomic, strong) NSString *cardnameLatin;
@property (nonatomic, strong) NSString *cardContentHTML;
@property (nonatomic, strong) NSArray *temperatureArray;

//loading data
- (void) retrieveData;

//clicking anywhere to go back previous screen
- (IBAction)backgroundClick:(id)sender;


@end
