//
//  PhyloPublicGamesViewController.m
//  Phylo
//
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPublicGamesViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloCheckpoints.h"
#import "PhyloQuiz.h"
#import "PhyloCard.h"

@interface PhyloPublicGamesViewController ()

@end

@implementation PhyloPublicGamesViewController{
    //Instance Variables: CurrentTableData, FilteredSearchResults, and a boolean if the table's being filtered
    NSMutableArray *tableData;
    NSMutableArray *searchResults;
    BOOL isFiltered;
}
@synthesize search;
@synthesize searchesTable;

- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

    //Customer Constructor
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Public Games", @"Public Games");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
	//Function that is called when this page is loaded
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set Delegate
    self.search.delegate = self;
    self.searchesTable.delegate = self;
    self.searchesTable.dataSource = self;
    
    [self changeBackButton];
    
    
    

}
/*----------------------------------------------
 Pulls all public games and put them into a table
 ----------------------------------------------*/
- (void) viewWillAppear:(BOOL)animated{
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getGamesInfo.php?type=public"];
    
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    
    
    NSArray* GamesStringArr = [strResult componentsSeparatedByString:@"<br>"];
    
    tableData = [[NSMutableArray alloc]init];
    
    for (int i = 0;i<[GamesStringArr count]-1;i++){
        NSArray* gameInfoArr = [[GamesStringArr objectAtIndex:i]componentsSeparatedByString:@":"];
        NSArray* temparr = [[gameInfoArr objectAtIndex:2] componentsSeparatedByString:@","];
        NSMutableArray* chkpointArr = [[NSMutableArray alloc]initWithArray:temparr];
        PhyloGames* tempGame = [[PhyloGames alloc]initNewGame:gameInfoArr[0] withCheckpoints:chkpointArr];
        
        [tableData addObject:tempGame];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    //# of sections for tabledata. We only need 1.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFiltered)
        return [searchResults count];
    return [tableData count];
    
}

//Customize the appearance of table view cells. It is called for every cell object
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //set up the cell data
    //JACKY'S VERSION 2013/06/22
    PhyloGames* temp = [[PhyloGames alloc]init];
    if (!isFiltered){
        temp = [tableData objectAtIndex:indexPath.row];
        cell.textLabel.text = temp.gameName;
    }
    else{ //Searching for particular string
        temp= [searchResults objectAtIndex:indexPath.row];
        cell.textLabel.text= temp.gameName;
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

/*----------------------------------------------
When a game is selected, pull the corresonding game information and update the tempGame in updateContainer. Finally, proceed to a mapview (which will load that tempGame for processing). 
 ----------------------------------------------*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //update Temp Game
    PhyloGames* newGame = [tableData objectAtIndex:indexPath.row];
    
    //Prepare a string (which contains the checkpoints' IDs) for php
    NSString *chkpointStr = [NSString stringWithFormat:@"chkpoint[]=%d",[[newGame.setOfCheckpoints objectAtIndex:0]intValue]];
    for (int i=1;i<[newGame.setOfCheckpoints count];i++){
        chkpointStr =[NSString stringWithFormat:@"%@&chkpoint[]=%d",chkpointStr,[[newGame.setOfCheckpoints objectAtIndex:i]intValue]];
    }
    //Pull checkpoints from database
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getCheckpointsInfo.php?%@",chkpointStr];
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    NSArray* chkPointArrUnparsed = [strResult componentsSeparatedByString:@"<br>"];
    
    //The following routine creates a checkpointarray that PhyloGames and MapViewCon can use; using the data extracted from database.
    NSMutableArray* newChkpointArr = [[NSMutableArray alloc]init];
    for (int i=0;i<[chkPointArrUnparsed count]-1;i++){
        
        NSArray* chkPointInfoArr = [[chkPointArrUnparsed objectAtIndex:i]componentsSeparatedByString:@"|"];
        CLLocationCoordinate2D tempCoor;
        tempCoor.latitude = (double) [chkPointInfoArr[2] floatValue];
        tempCoor.longitude = (double) [chkPointInfoArr[3] floatValue];
        NSArray* quizQs = [[NSArray alloc]initWithObjects:chkPointInfoArr[6],chkPointInfoArr[7],chkPointInfoArr[8],chkPointInfoArr[9], nil];
        
        strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getCardFromID.php?cardID=%d",[chkPointInfoArr[4] intValue]];
        NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
        NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
        NSArray* cardInfo = [strResult componentsSeparatedByString:@"@"];
        
        NSURL *url = [NSURL URLWithString:cardInfo[3]];
        NSData *parsed = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:parsed] ;
        
        PhyloCard* tempCard = [[PhyloCard alloc]init:cardInfo[0] withWebPath:cardInfo[1] withCardID:[cardInfo[2] intValue] withImage:img];
        PhyloQuiz* tempQuiz = [[PhyloQuiz alloc]init:chkPointInfoArr[5] withAnswers:quizQs awardingpoints:[chkPointInfoArr[12]intValue] correctAnswerID:[chkPointInfoArr[10]intValue] withImage:img];
        PhyloCheckpoints* tempChkpoint = [[PhyloCheckpoints alloc]initNewCheckpoint:tempCoor withName:chkPointInfoArr[1] withQuiz:tempQuiz withCard:tempCard];
        
        
        
        [newChkpointArr addObject:tempChkpoint];
    }
    
    newGame.setOfCheckpoints = newChkpointArr;
    
    [self theAppDataContainer].tempGame = newGame;
    
    PhyloMapViewController *mapViewController = [[PhyloMapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [self.navigationController pushViewController:mapViewController animated:YES];
}


    //dismiss keyboard
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.search resignFirstResponder];
    
    
    
}

/*----------------------------------------------
 The following methods are used for search filters.
 ----------------------------------------------*/


#pragma mark Content Filtering

    //filtering function
    -(void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope{
    [searchResults removeAllObjects];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF.gameName contains[c] %@",searchText];
    searchResults = [NSMutableArray arrayWithArray:[tableData filteredArrayUsingPredicate:predicate]];
}

//Search Delegates



//SearchDisplayController Delegates
-(BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:searchOption]];
    return YES;
}

//Function that filters the table view content.
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length==0)
        isFiltered = NO;
    else{
        isFiltered = YES;
        searchResults = [[NSMutableArray alloc]init];
        for (PhyloGames *games in tableData){
            NSRange stringRange = [games.gameName rangeOfString:searchText options:NSCaseInsensitiveSearch ];
            if (stringRange.location!= NSNotFound)
                [searchResults addObject:games];
            
        }
    }
    [self.searchesTable reloadData];
    
}


//Reset the searchBar's input.
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@"";
    
    searchResults = [tableData mutableCopy];
    [self.searchesTable reloadData];
    
    [searchBar resignFirstResponder];
    
}

-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
