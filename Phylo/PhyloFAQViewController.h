//
//  PhyloFAQViewController.h
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-27.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloFAQViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITextView *textview;

- (IBAction)backButtonClicked:(UIButton*)sender;
@end
