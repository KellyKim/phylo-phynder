//
//  MapViewAnnotaion.m
//  Phylo
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloMapViewAnnotation.h"

@implementation PhyloMapViewAnnotation

@synthesize title, coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
	if (!(self = [super init])) return nil;
	//initializing variables
	title = ttl;
	coordinate = c2d;
	return self;
}


@end