//
//  PhyloTabBarController.h
//  Phylo
//
//  Created by Jacky Chao on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloTabBarController : UITabBarController

@end
