//
//  PhyloPlayViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPlayViewController.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloHelpViewController.h"
#import "PhyloDataContainer.h"
#import "QuartzCore/QuartzCore.h"	
#import "PhyloExtraFunctions.h"

@interface PhyloPlayViewController ()

@end

@implementation PhyloPlayViewController

/*----------------------------------------------
 App Container
 ----------------------------------------------*/
- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
     if (self) {
         //Custom initialization
        self.title = NSLocalizedString(@"Play", @"Play"); //the text under the tab
    }
    return self;
}

- (void)viewDidLoad
{
    // Changes the background image
    UIImageView* backgroundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LogInBackground.png"]];
    [backgroundImage setFrame:self.view.frame];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage.image]];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

/*----------------------------------------------
Because this view will appear automatically after a game has been completed in PhyloMapViewCon, an alert (with sound) should pop if the game was successfully completed (all checkpoints visited.) Subsequentially, the user's gameCompleted variable in both the datacontainer and the database will be incremented. 
 It should not pop the game was completed or there was no game to begin with.
 ----------------------------------------------*/
- (void)viewWillAppear:(BOOL)animated{
    if ([self theAppDataContainer].tempGame == nil){}
    else if ([self theAppDataContainer].tempGame.completed == YES){
        
        [self theAppDataContainer].soundEnabled? [PhyloExtraFunctions playSound:@"ROMVP" withType:@"mp3"] : nil;
        
        [[[UIAlertView alloc]initWithTitle:@"Congratualations!" message:@"You have completed this game!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        [self theAppDataContainer].gamesCompleted++;
        
        NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/updateData.php?UserName=%@&newVal=%d&destination=%@", [self theAppDataContainer].userName,[self theAppDataContainer].gamesCompleted  ,@"Total+Games+Played"];
        
        NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
        
        NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
        
        NSLog(@"Total games %@",strResult);
        
        [self theAppDataContainer].tempGame.completed = NO;
    }
}

- (void)didReceiveMemoryWarning
{	
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*----------------------------------------------
 The following 3 methods execute when users click [Public Games], [Private Games], or [Information]. They bring the users to the corresponding ViewCon.
 ----------------------------------------------*/
-(IBAction)publicGamePressed:(id)sender{
    //change to the public games view controller
    //prepare the public games view
    PhyloPublicGamesViewController *publicGamesViewController = [[PhyloPublicGamesViewController alloc] initWithNibName:@"PhyloPublicGamesViewController" bundle:nil];
    //display the public games view, which contains a list of all the public games
    [self.navigationController pushViewController:publicGamesViewController animated:YES];
}

-(IBAction)privateGamePressed:(id)sender{
    //prepare the private games view
    PhyloPrivateGamesViewController *privateGamesViewController = [[PhyloPrivateGamesViewController alloc] initWithNibName:@"PhyloPrivateGamesViewController" bundle:nil];
    //display the private games, which contains a list of all of the private games
    [self.navigationController pushViewController:privateGamesViewController animated:YES];
}


-(IBAction)helpGamePressed:(id)sender{
    //prepare the help games view
    PhyloHelpViewController *HelpViewController = [[PhyloHelpViewController alloc] initWithNibName:@"PhyloHelpViewController" bundle:nil];
    //display the private games, which contains a list of all of the private games
    [self.navigationController pushViewController:HelpViewController animated:YES];
}

@end
