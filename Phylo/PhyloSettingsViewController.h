//
//  PhyloSettingsViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloSettingsViewController : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *logOutButton;

- (IBAction)logOut:(UIButton *)sender;
- (IBAction)accountSettingsPressed:(UIButton *)sender;

@end
