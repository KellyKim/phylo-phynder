//
//  PhyloCardCollectionViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//


#import "PhyloCardCollectionViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PhyloCardViewController.h"

@interface PhyloCardCollectionViewController ()
{
    NSMutableArray *cards;
}
@end

@implementation PhyloCardCollectionViewController
@synthesize cardURLs,cardName,cardImage, cardTable;

- (PhyloDataContainer*) theAppDataContainer{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"My Cards", @"My Cards"); //the text under the tab
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.cardTable.delegate = self;
    self.cardTable.dataSource = self;
    
    // Note from Jacky 2013/07/05
    // we might as well load card collection into data container class as soon as user is logged in. Server side data will be updated when user acquires a card, and, after reaquiring such data, the container will refresh in this view through viewWillAppear.
    [self changeBackButton];
    
}

- (void)viewWillAppear:(BOOL)animated{
    PhyloDataContainer* userDataContainer = [self theAppDataContainer];
    
    
    // read cardcollection.txt
    
    
    
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getCards.php?userName=%@", userDataContainer.userName];
    
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    
    NSArray *userCards = [strResult componentsSeparatedByString:@"!"];
    
    cards = [[NSMutableArray alloc] init];
    
    cardURLs = [NSMutableArray arrayWithArray:userCards];
    
    cardName = [[NSMutableArray alloc] init];
    
    cardImage = [[NSMutableArray alloc] init];
    
    
    //extracts the different card details as returned by the .php script (getCards.php) so that displaying them on each tableView cell will be easy.
    for(int i = 0; i < [cardURLs count]; i++)
    {
        //store the card name and image in different arrays so that they are easily placed in each cell
        NSArray *cardNameAndImage = [cardURLs[i] componentsSeparatedByString:@"@"];
        cardName[i] = cardNameAndImage[0];
        cardImage[i] = cardNameAndImage[2];
        
    }
    
    
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self theAppDataContainer].cardsCollected; //the amount of rows in the tableView will be the amount of cards in the user's card collection
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *cellCardImage = [cardImage objectAtIndex:indexPath.row];
    NSString *cellCardName = [cardName objectAtIndex:indexPath.row];
    
    //make table cell text display the animal/organism name
    cell.textLabel.text = cellCardName;
    
    //make table cell image/thumbnail display the animal after it finishes loading, but still show a default image if connection is slow
    UIImageView *addImage = [[UIImageView alloc] init];
    [addImage setImageWithURL:[NSURL URLWithString:cellCardImage]];
    [addImage setFrame:CGRectMake(0, 0, 60,43)];
    [cell.contentView addSubview:addImage];
    [cell setIndentationLevel:6];
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Selected index: %ld", (long)indexPath.item);
    
    //we're interested in the card data that we pressed the button of. ((UIButton *)sender).tag gives us the ID of the button that we pressed as we defined it in the for(;;) loop earlier, and using that we can find the exact card we are looking for.
    NSString *individualCardInfo = cardURLs[(long)indexPath.item];
    
    NSLog(@"%@", individualCardInfo);
    
    //[((UIButton *)sender).tag]
    
    //prepare the card details screen
    PhyloCardViewController *cardViewController = [[PhyloCardViewController alloc] initWithNibName:@"PhyloCardViewController" bundle:nil];
    //change the URLGoTo value from the card details screen so that when the UIWebView of the next screen loads, it loads to the URL of the corresponding button.
    NSArray *individualCardURL = [individualCardInfo componentsSeparatedByString:@"@"];
    //cardViewController.URLGoTo = individualCardURL[1];
    
    //change the detailsTitle value from the card details screen so that when the next screen loads, the title is set to the title of the corresponding button we just pressed.
    //NSLog(@"%@", individualCardInfo[0]);
    cardViewController.cardname = individualCardURL[0];
    cardViewController.permLink = individualCardURL[1];
    
    //push the next screen onto the stack and display it
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    cardViewController.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self presentViewController:cardViewController animated:YES completion:nil];
}

-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
