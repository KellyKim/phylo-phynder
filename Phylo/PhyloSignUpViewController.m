//
//  PhyloSignUpViewController.m
//  Phylo
//
//  Created by Yun Zeng on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloSignUpViewController.h"
#import "PhyloLoginViewController.h"

@interface PhyloSignUpViewController ()

@end

@implementation PhyloSignUpViewController
@synthesize UserNameField,PasswordField,Re_PasswordField,PubOrPriLabel,PubOrPri;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//loading specifics of the screen including buttons and text field etc
- (void)viewDidLoad
{
    [super viewDidLoad];
    [PubOrPriLabel setText:[NSString stringWithFormat:@"User Profile's Privacy"]];
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"LogInBackground.png"]]];   
    // Do any additional setup after loading the view from its nib.

    [self.UserNameField setValue:[UIColor darkGrayColor]
                    forKeyPath:@"_placeholderLabel.textColor"];

    
    [self.PasswordField setValue:[UIColor darkGrayColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.Re_PasswordField setValue:[UIColor darkGrayColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//when sign up button is pressed
- (IBAction)SignUp:(id)sender {
    
    //pop up message
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign Up Success!" message:@"You can now log in." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    int privacyControl = (PubOrPri.selectedSegmentIndex==0)? 1 : 0;
    NSRange whiteSpaceRange1 = [UserNameField.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    NSRange whiteSpaceRange2 = [PasswordField.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    
    //make sure no space in user name/password
    if (whiteSpaceRange1.location != NSNotFound || whiteSpaceRange2.location != NSNotFound){
        alert.title = @"Oops!";
        alert.message = @"Please make sure that your username or password does not contain any white spaces.";
        [alert show];
        return;
    }
    
    //checking empty text field
    if (![UserNameField.text isEqualToString:@""] && [PasswordField.text isEqualToString:Re_PasswordField.text] && ![PasswordField.text isEqualToString:@""]){
    // create string contain url to signup.php in the server
    
        NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/UserSignUp.php?userName=%@&password=%@&UserPubOrPri=%d",
                            UserNameField.text, PasswordField.text,privacyControl];
        
        //excute php
        NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
        
        //receive returned value by php file
        NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@", strResult);

        if([strResult isEqualToString:@"Sign up Success"]){
            [self dismissViewControllerAnimated:YES completion:nil];
            [alert show];
        }else
        {
            alert.title = @"Oops!";
            alert.message = @"This ID already exist. Please Enter a different one.";
            [alert show];
        }
        
    }

    else{
        alert.title = @"Oops!";
        alert.message = @"Please check to make sure you have entered the same password twice and a User name";
        [alert show];
    }
}

//back to login screen if cancel sign up
- (IBAction)Cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//dismiss keyboard when background is clicked
- (IBAction)backgroundClick:(id)sender{

    [UserNameField resignFirstResponder];
    [PasswordField resignFirstResponder];
    [Re_PasswordField resignFirstResponder];
    
}


@end
