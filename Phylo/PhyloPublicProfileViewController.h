//
//  PhyloPublicProfileViewController.h
//  Phylo
//
//  Created by Cody on 2013-07-19.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloPublicProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *infoTable;

@property (nonatomic, strong) NSMutableArray *profileInfo;

@end
