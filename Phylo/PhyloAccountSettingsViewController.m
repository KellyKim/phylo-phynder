//
//  PhyloAccountSettingsViewController.m
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloAccountSettingsViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegate.h"

@interface PhyloAccountSettingsViewController (){
    UIAlertView* b4Delete;
}

@end

@implementation PhyloAccountSettingsViewController

- (PhyloDataContainer*) theAppDataContainer{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //setting title of the screen
        self.title = NSLocalizedString(@"Account Settings", @"Account Settings");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self changeBackButton]; //change back button from default to our own image
}

-(IBAction)deleteAccountPressed:(UIButton *)sender{ //Delete account button was pressed
    //should do some confirmation here.
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"Deleting your account deletes all of your progress, and cannot be undone." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [alert setTag:1];
    [alert show];
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonindex{
    NSLog(@"%d",alertView.tag);
    if (alertView.tag==1){
    //nothing will be done when buttonindex==0, which is cancel
        if (buttonindex == 1){
            NSLog(@"DELETE MY ACCOUNT");
            
            //delete the user's account after entering the correct password
            b4Delete = [[UIAlertView alloc]initWithTitle:@"Please enter your password." message:@"We need to make sure you are the actual user." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
            b4Delete.alertViewStyle = UIAlertViewStyleSecureTextInput;
            [b4Delete textFieldAtIndex:0].delegate = self;
            [b4Delete setTag:2];
            [b4Delete show];
        }
    }
    
        else if (alertView.tag==2){
            if (buttonindex==1 && [[alertView textFieldAtIndex:0].text isEqualToString:[self theAppDataContainer].userPassword ]){
        NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/deleteProfile.php?userName=%@", [self theAppDataContainer].userName];
        NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
        NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
        NSLog(@"%@",strResult);
        
        PhyloLoginViewController *logInController = [[PhyloLoginViewController alloc]init];
        
        UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
        logInController.modalTransitionStyle = animation;
        [UIView setAnimationTransition:animation forView:	self.view cache:YES];
            [self presentViewController:logInController animated:YES completion:nil];
            }
            else if (buttonindex==0){}
            
            else{
                UIAlertView* notCorrect = [[UIAlertView alloc]initWithTitle:@"Incorrect!" message:@"Your password is incorrect! Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [notCorrect show];
            }
            }
        
    }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeBackButton{ //change the back button image to the one we designed
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{ //pop the current view
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchChanged:(UISwitch*)sender {
    [self theAppDataContainer].soundEnabled = sender.on? YES : NO;
        
    
}
@end
