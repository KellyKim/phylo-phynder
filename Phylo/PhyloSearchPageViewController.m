//
//  PhyloSearchPageViewController.m
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloSearchPageViewController.h"
#import "PhyloPublicProfileViewController.h"

@interface PhyloSearchPageViewController ()

@end

@implementation PhyloSearchPageViewController
@synthesize profileInfo, profileNames, profileTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //load title of search screen for public profiles
        self.title = NSLocalizedString(@"Profile Search", @"Profile Search"); //the text under the tab
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    //connection string of database
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/searchPublicProfiles.php"];
    
    //retrieve result from database
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    //convert into string
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",strResult);
    
    NSArray *userProfiles = [strResult componentsSeparatedByString:@"<br>"];
    
    profileInfo = [NSMutableArray arrayWithArray:userProfiles];
    
    profileNames = [[NSMutableArray alloc] init];
    
    //takes all of the data in each index of userProfiles and stores the name of the current profile into an array for easy displaying in each tableView cell.
    for(int i = 0; i < [profileInfo count] - 1; i++)
    {
    //load string into array
        NSRange range = [profileInfo[i] rangeOfString:@":"];
        NSString *substring = [profileInfo[i] substringToIndex:range.location];
        NSLog(@"Location of ':': %lu", (unsigned long)range.location);
        NSLog(@"New substring: %@", substring);
        profileNames[i] = substring;
    }
    
    NSLog(@"profileNames count = %d", [profileNames count]);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //load table
    self.profileTable.delegate = self;
    self.profileTable.dataSource = self;
    
    [self changeBackButton];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [profileNames count];
}

//display of each cell in the table
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSLog(@"Check if indexing is correct: %@", [profileNames objectAtIndex:indexPath.row]);
    
    NSString *cellValue = [profileNames objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Selected index: %ld", (long)indexPath.item);
    
    //we're interested in the card data that we pressed the button of. ((UIButton *)sender).tag gives us the ID of the button that we pressed as we defined it in the for(;;) loop earlier, and using that we can find the exact card we are looking for.
    NSString *individualProfileInfo = profileInfo[(long)indexPath.item];
    
    NSLog(@"%@", individualProfileInfo);
    
    //prepare the card details screen
    PhyloPublicProfileViewController *publicProfileViewController = [[PhyloPublicProfileViewController alloc] initWithNibName:@"PhyloPublicProfileViewController" bundle:nil];
    //change the URLGoTo value from the card details screen so that when the UIWebView of the next screen loads, it loads to the URL of the corresponding button.
    NSArray *individualProfileArray = [individualProfileInfo componentsSeparatedByString:@":"];
    //[publicProfileViewController.profileInfo addObjectsFromArray:individualProfileArray];
    publicProfileViewController.profileInfo = (NSMutableArray*)individualProfileArray;
    
    //push the next screen onto the stack and display it
    [self.navigationController pushViewController:publicProfileViewController animated:YES];
}

//back to previous screen with a back button
-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
