/*
 PhyloAppDeletegate: Copyright@TerraByte
 
 This class serves as the root core for PhyloPhynder. It creates the necessary starting UIViewControllers and data container. It also redesigns some of the UI in each UIViewController programmatically.

 Last Update: 2013/07/27
 
 */

#import <UIKit/UIKit.h>
#import "PhyloLoginViewController.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloDataContainer.h"

@class PhyloMapViewController;
@interface PhyloAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate,PhyloAppDelegateProtocol>{
}

@property (strong, nonatomic) UIWindow *window; //app window
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) PhyloDataContainer *theAppDataContainer; //the datacontainer that shall be shared through app lifetime
@property (strong, nonatomic) NSArray *viewsArray;






@end
