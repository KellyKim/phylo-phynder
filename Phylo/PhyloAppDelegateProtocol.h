//
//  PhyloAppDelegateProtocol.h
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//


#import <Foundation/Foundation.h>

@class AppDataContainer;

@protocol PhyloAppDelegateProtocol 

-(AppDataContainer*) theAppDataContainer;

@end

