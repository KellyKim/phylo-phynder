//
//  PhyloCardAttributes.m
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-24.
//  Copyright (c) 2013 Terrabyte. All rights reserved.
//

#import "PhyloCardAttributes.h"

@implementation PhyloCardAttributes
@synthesize artURL,backgroundURL,hierachyURL,sizeURL,name;


-(PhyloCardAttributes*) initWithCardname:(NSString*)cardname andBackGroundurl:(NSString*)BGurl andArturl:(NSString*)ATRurl andSizeurl:(NSString*)SIZEurl andHierachyurl:(NSString*)HIERACHYurl{
    self = [super init];
    
    //save all the input variable into onc object as cardattribute
    if(self){
    self.name = cardname;
    self.backgroundURL = BGurl;
    self.artURL = ATRurl;
    self.sizeURL = SIZEurl;
    self.hierachyURL = HIERACHYurl;
    }
    return self;
    
}

@end
