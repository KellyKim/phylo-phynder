//
//  PhylohelpHowToViewController.m
//  Phylo
//
//  Created by Michael Riyanto on 2013-07-14.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloHowToViewController.h"

@interface PhyloHowToViewController ()

@end

@implementation PhyloHowToViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [scroller setScrollEnabled:YES];
    [scroller setContentSize:CGSizeMake(320, 600)];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender { //back button was pressed
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    self.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self dismissViewControllerAnimated:YES completion:nil]; //dismiss the current view
}

@end
