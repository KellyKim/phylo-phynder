//
//  PhyloCard.m
//  Phylo
//
//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import "PhyloCard.h"

@implementation PhyloCard
@synthesize cardName;
@synthesize webPath;
@synthesize cardIdentifier;
@synthesize cardImage;

-(PhyloCard*)init:(NSString*)name withWebPath:(NSString*)webpath withCardID:(int)ID withImage:	(UIImage*)image{
    self = [super init];
    
    if (self){
    //initializing card attributes
        self.cardName = name;
        self.webPath = webpath;
        self.cardIdentifier = ID;
        self.cardImage = image;
    }
    return self;
}

@end
