//
//  PhyloSearchPageViewController.h
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloSearchPageViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *profileTable;

@property (nonatomic, strong) NSMutableArray *profileInfo;
@property (nonatomic, strong) NSMutableArray *profileNames;

@end
