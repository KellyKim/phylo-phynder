//
//  Quiz_GameViewController.h
//  Quiz Game
//
//

#import <UIKit/UIKit.h>
#import "PhyloQuiz.h"

@interface PhyloQuizViewController : UIViewController<UIAlertViewDelegate> {


    IBOutlet	UIImageView	*imageView;
    

    
	NSInteger myScore;
	NSInteger questionNumber;
	NSInteger rightAnswer;
	NSInteger time;
	NSArray *theQuiz;
	NSTimer *timer;
	BOOL questionLive;
    BOOL nextGame;

}

@property (retain, nonatomic) UILabel	*theQuestion;
@property (retain, nonatomic) UILabel	*theScore;
@property (retain, nonatomic) UIButton	*answerOne;
@property (retain, nonatomic) UIButton	*answerTwo;
@property (retain, nonatomic) UIButton	*answerThree;
@property (retain, nonatomic) UIButton	*answerFour;
@property (retain, nonatomic) NSArray *theQuiz;
@property (retain, nonatomic) NSTimer *timer;
@property (retain, nonatomic) UIButton *extra;
@property (retain, nonatomic) PhyloQuiz* quiz;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;




-(IBAction)buttonOne;
-(IBAction)buttonTwo;
-(IBAction)buttonThree;
- (IBAction)dismissThisView:(UIButton *)sender;





@end

