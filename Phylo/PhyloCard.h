//
//  PhyloCard.h
//  Phylo

//  This class implements a card object that will be attached to every checkpoint object. It contains the cardname, the card's web path, card's ID (unique to each card), and the card's image. It contains the most basic initialization method.

//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloCard : NSObject
{
}

@property (nonatomic,strong) NSString* cardName;
@property (nonatomic,strong) NSString* webPath;
@property (nonatomic,assign) int cardIdentifier;
@property (nonatomic,strong) UIImage* cardImage;



-(PhyloCard*)init:(NSString*)name withWebPath:(NSString*)webpath withCardID:(int)ID withImage:(UIImage*)image;

@end
