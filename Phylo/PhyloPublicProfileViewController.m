//
//  PhyloPublicProfileViewController.m
//  Phylo
//
//  Created by Cody on 2013-07-19.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPublicProfileViewController.h"

@interface PhyloPublicProfileViewController ()

@end

@implementation PhyloPublicProfileViewController
@synthesize profileInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIImageView* backgroundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LogInBackground.png"]];
    [backgroundImage setFrame:self.view.frame];
    
    self.infoTable.backgroundView = backgroundImage;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = [NSString stringWithFormat:@"%@'s Profile:", profileInfo[0]]; //set the title of the view to be dynamic according to the profile being viewed.
    NSLog(@"%@",profileInfo);
    
    [self changeBackButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { //number of sections in table
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName;
    switch(section){
        case 0://name of the first table section. we only have one section for now, so other values will default to nothing. section name is dynamic according to the current profile being viewed.
            sectionName = [NSString stringWithFormat:@"%@'s Profile:", profileInfo[0]];
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {//
    static NSString *CellIdentifier = @"Cell";
    NSString *cards = profileInfo[1];
    NSArray *cardsArray = [cards componentsSeparatedByString:@"!"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    //set up the cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0){ //there is, without a doubt, a more efficient way to implement row items.
        cell.textLabel.text = [NSString stringWithFormat:@"Cards collected:"]; //text to the left of the cell is "Cards collected:"
        NSLog(@"number of cards: %d", [cardsArray count]);
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [cardsArray count]]; //text to the right of the cell is the amount of cards the searched profile has in his/her digital collection
    }
    else if ([indexPath row] == 1){
        cell.textLabel.text = [NSString stringWithFormat:@"Total points:"]; //left cell text is "Total points:"
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",profileInfo[2]]; //right cell text is the amount of points the searched profile has
    }
    else if ([indexPath row] == 2){
        cell.textLabel.text = [NSString stringWithFormat:@"Total number of games played:"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",profileInfo[3]];
    }
    else if ([indexPath row] == 3){
        cell.textLabel.text = [NSString stringWithFormat:@"Member since:"];
        cell.detailTextLabel.text = profileInfo[4];
    }
    
    return cell;
}

-(void)changeBackButton{ //changes the back button image for a consistent UI
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
