//
//  PhyloHelpViewController.h
//  Phylo
//
//  Created by Michael Riyanto on 7/11/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "PhyloMapViewController.h"

@interface PhyloHelpViewController : UIViewController{

    IBOutlet	UIImageView	*imageView;


}
    



@property (nonatomic, retain) IBOutlet UIButton *Phylophynder;
@property (nonatomic, retain) IBOutlet UIButton *HowToPlay;
@property (nonatomic, retain) IBOutlet UIButton *PhyloCard;
@property (nonatomic, retain) IBOutlet UIButton *Credit;


- (IBAction)Phylophynder:(id)sender;
- (IBAction)HowToPlay:(id)sender;
- (IBAction)Credit:(id)sender;


@end


