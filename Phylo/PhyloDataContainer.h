//
//  PhyloDataContainer.h
//  Phylo
//
//  This file serves as a data container which will be used by most views in the project. It takes an protocol method from AppDataContainer. Feel free to add objects you think are unique to user profiles.
//  Created by Jia Jiu Chao on 2013-07-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "AppDataContainer.h"
#import "PhyloCard.h"
#import "PhyloQuiz.h"
#import "PhyloGames.h"
#import "PhyloCheckpoints.h"

@interface PhyloDataContainer : AppDataContainer

@property (nonatomic,strong) NSString* userName;
@property (nonatomic,strong) NSString* userPassword;
@property (nonatomic,assign) BOOL loggedIn;
@property (nonatomic,assign) NSInteger cardsCollected; //pull from database
@property (nonatomic,assign) NSInteger points; //pull from database
@property (nonatomic,assign) NSInteger gamesCompleted; //^
@property (nonatomic,assign) NSInteger privateOrPublic; //^
@property (nonatomic,strong) NSString* dateJoined;
@property (nonatomic,assign) BOOL soundEnabled;


//these properties help views communicate
@property (nonatomic,strong) PhyloGames* tempGame;
@property (nonatomic,strong) PhyloQuiz* tempQuiz;
@property (nonatomic,strong) PhyloCard* tempCard;
@property (nonatomic,strong) PhyloCheckpoints* tempCheckpoints;

@end
