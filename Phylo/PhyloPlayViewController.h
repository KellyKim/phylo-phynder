//
//  PhyloPlayViewController.h
//  Phylo
/*----------------------------------------------
 This class implements the first screen that appears after the user has logged in. It takes the user to the PublicGameTable, PrivateGameTable, or HelpScreen.
 ----------------------------------------------*/
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloPublicGamesViewController.h"
#import "PhyloPrivateGamesViewController.h"
#import "PhyloHelpViewController.h"



@interface PhyloPlayViewController : UIViewController{
    IBOutlet	UIImageView	*imageView;
}

@property (strong, nonatomic) IBOutlet UIButton *pubGames;
@property (strong, nonatomic) IBOutlet UIButton *priGames;
@property (strong, nonatomic) IBOutlet UIButton *help;


-(IBAction)publicGamePressed:(id)sender;
-(IBAction)privateGamePressed:(id)sender;
-(IBAction)helpGamePressed:(id)sender;

@end
