//
//  PhyloCardViewController.m
//  Phylo
//
//  Created by Yun Zeng on 7/14/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCardViewController.h"

@interface PhyloCardViewController ()

@end



@implementation PhyloCardViewController
@synthesize json, cardAttributes, cardname, permLink, cardnameLatin, cardContentHTML, temperatureArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //load page
    [super viewDidLoad];
    
    //retrieve card data from json api online
    [self retrieveData];
    
    //loading backgroud image
    NSLog(@"loading image");
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:cardAttributes.backgroundURL]];
    self.background.image = [UIImage imageWithData:imageData];
    
    //loading art
    NSData *imageData2 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cardAttributes.artURL]];
    self.art.image = [UIImage imageWithData:imageData2];
    
    //loading other attributes
    NSData *imageData3 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cardAttributes.hierachyURL]];
    self.foodhierachy.image = [UIImage imageWithData:imageData3];
    
    NSData *imageData4 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cardAttributes.sizeURL]];
    self.size.image = [UIImage imageWithData:imageData4];
    
    self.name.text = cardname;
    [self.name setBackgroundColor:[UIColor clearColor]];
    
    self.nameLatin.text = cardnameLatin;
    [self.nameLatin setBackgroundColor:[UIColor clearColor]];
    
    [self.cardContent loadHTMLString: (cardContentHTML) baseURL: nil];
    [self.cardContent setBackgroundColor:[UIColor clearColor]];
    
    NSLog(@"temperatureArray: %@", temperatureArray);
    
    
    //to catch empty array exception
    //exception happens when cards with no temperature attribute are loaded
    @try
    {
        
        if([temperatureArray count] == 0)
        {
            self.temperature.text = @"";
        }
        else if([temperatureArray count] == 1)
        {
            self.temperature.text = temperatureArray[0];
        }
        else if([temperatureArray count] == 2)
        {
            self.temperature.text = [NSString stringWithFormat:@"%@, %@", temperatureArray[0], temperatureArray[1]];
        }
    }
    @catch (NSException * e)
    {
        self.temperature.text = @"";
    }
    @finally {
        //self.temperature.text = @"";
    }
//    imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:cardAttributes.artURL]];
//    self.art.image = [UIImage imageWithData:imageData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) retrieveData
{
    

    //append the URL of the card with api=json to retrieve json object
    NSString *strURL = [NSString stringWithFormat: @"%@?api=json",permLink];
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    NSLog(@"retrieving data1");
    if(data != nil){//if the link is correct, there should be data
        NSLog(@"retrieving data2");
        //parsing the json object
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        //saving the data into variables
        NSString *BGurl = [[json objectAtIndex:0] objectForKey:@"background_image_url"];
        NSString *ARTurl = [[json objectAtIndex:0] objectForKey:@"graphic"];
        NSString *SIZEurl = [[json objectAtIndex:0] objectForKey:@"size_image_url"];
        NSString *HIERACHYurl = [[json objectAtIndex:0] objectForKey:@"food_hierarchy_image_url"];
        cardnameLatin = [[json objectAtIndex:0] objectForKey:@"latin_name"];
        cardContentHTML = [[json objectAtIndex:0] objectForKey:@"card_content"];
        temperatureArray = [[json objectAtIndex:0] objectForKey:@"temperature"];

        cardAttributes = [[PhyloCardAttributes alloc] initWithCardname:cardname andBackGroundurl:BGurl andArturl:ARTurl andSizeurl:SIZEurl andHierachyurl:HIERACHYurl];
    }
}

- (IBAction)backgroundClick:(id)sender{
    
    //flip to previous screen when clicked anywhere in the current page
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    self.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
