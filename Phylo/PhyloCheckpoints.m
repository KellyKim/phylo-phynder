//
//  PhyloCheckpoints.m
//  Phylo
//
//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import "PhyloCheckpoints.h"

@implementation PhyloCheckpoints
@synthesize locationCoordinate;
@synthesize quiz;
@synthesize card;
@synthesize visited;
@synthesize checkpointTitle;

- (PhyloCheckpoints*)initNewCheckpoint:(CLLocationCoordinate2D)atCoordinate withName:(NSString*)name withQuiz:(PhyloQuiz*)chkpointQuiz withCard:(PhyloCard*)chkpointCard{
    
    self = [super init];
    
    if(self){
    //initialize quizes and cordinates variables associated with check point
        locationCoordinate = atCoordinate;
        checkpointTitle = name;
        quiz = chkpointQuiz;
        card = chkpointCard;
        visited = NO;
        
    }
    return self;
    
}

// This method is replaced by MapViewController's annotation methods
- (void)markOffCheckpoint{
    // quiz view controller is going to trigger a button that sets arbitrary variable completed in quiz class to false, which will be passed here
    
    //if (_quiz.completed ==YES)
    //_visited = YES;
    
    //with _visited set to YES, users cannot reenter this checkpoint.
}
// This method converts a locationcoordinate to a location obj.
- (CLLocation*)locationFormat{
    CLLocation* tempLocation = [[CLLocation alloc]initWithLatitude:locationCoordinate.latitude longitude:locationCoordinate.longitude];
    return tempLocation;
}

@end
