//
//  PhyloScavengerHuntViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloScavengerHuntViewController.h"
//#import "PhyloCreateViewController.h"

@interface PhyloScavengerHuntViewController ()

@end

@implementation PhyloScavengerHuntViewController

//@synthesize createGame;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Scavenger Hunt", @"Scavenger Hunt");
        self.tabBarItem.image = [UIImage imageNamed:@"first"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //[createGame addTarget:self action:@selector(createGameClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
}

- (IBAction)createGameClicked:(id)sender {
    
    
    [self.navigationController pushViewController:createViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
