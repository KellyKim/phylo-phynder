//
//  PhyloCheckpoints.h
//  Phylo
//  
//  This class implements a checkpoint object. It contains an unique PhyloQuiz, a PhyloCard, a coordinate struct, a title and a boolean status val. It also contains a initialization method as well as a location formatting method. 
//
//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <coreLocation/CoreLocation.h>
#import "PhyloQuiz.h"
#import "PhyloCard.h"

@interface PhyloCheckpoints : NSObject{

}

@property (nonatomic,assign) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic,strong) NSString* checkpointTitle;
@property (nonatomic,strong) PhyloQuiz* quiz;
@property (nonatomic,strong) PhyloCard* card;
@property (nonatomic,assign) BOOL visited;



- (PhyloCheckpoints*)initNewCheckpoint:(CLLocationCoordinate2D)atCoordinate withName:(NSString*)name withQuiz:(PhyloQuiz*)chkpointQuiz withCard:(PhyloCard*)chkpointCard;
- (void)markOffCheckpoint;
- (CLLocation*)locationFormat;


@end
