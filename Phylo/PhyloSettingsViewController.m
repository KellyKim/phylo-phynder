//
//  PhyloSettingsViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloSettingsViewController.h"
#import "PhyloAppDelegate.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloDataContainer.h"
#import "PhyloLoginViewController.h"
#import "PhyloAccountSettingsViewController.h"

@interface PhyloSettingsViewController ()

@end

@implementation PhyloSettingsViewController

- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Settings", @"Settings");
//        self.tabBarItem.image = [UIImage imageNamed:@"20-gear2.png"];
    }
    return self;
}

//loading screen
- (void)viewDidLoad
{

    UIImage *backgroundImage = [UIImage imageNamed:@"LogInBackground.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    //self.background_play = [UIImage imageNamed:@"background_play.png"];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)accountSettingsPressed:(UIButton *)sender{ //Account Settings button was pressed
    PhyloAccountSettingsViewController *accountSettingsViewController = [[PhyloAccountSettingsViewController alloc] initWithNibName:@"PhyloAccountSettingsViewController" bundle:nil]; //prepare the Account Settings view
    [self.navigationController pushViewController:accountSettingsViewController animated:YES]; //display the Account Settings view
}

//log out and quit the game
- (IBAction)logOut:(UIButton *)sender {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You will lose all your current game's progress." delegate:self cancelButtonTitle:@"Nah" otherButtonTitles:@"Bye!", nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonindex{

    //nothing will be done when buttonindex==0, which is cancel
    if (buttonindex == 1){
        //save user data to server... then reset device's profile to nil.
        
        PhyloDataContainer* updateContainer = [self theAppDataContainer];
        updateContainer.userName = nil;
        updateContainer.points = 0;
        updateContainer.tempGame = nil;
        [[self.view viewWithTag:111] removeFromSuperview];
        
    
        //present view with customized animations...
        PhyloLoginViewController *logInController = [[PhyloLoginViewController alloc]init]; //go back to the original log in screen.  Prepare the login view.
        
        UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
        logInController.modalTransitionStyle = animation; //specify the animation
        [UIView setAnimationTransition:animation forView:	self.view cache:YES];
        [self presentViewController:logInController animated:YES completion:nil]; //display the login view.
        
        
    }
}
@end
