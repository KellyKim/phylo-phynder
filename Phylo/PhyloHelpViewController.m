//
//  PhyloHelpViewController.m
//  Phylo
//
//  Created by Michael Riyanto on 7/11/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloHelpViewController.h"
#import "PhyloInformationViewController.h"
#import "PhyloHowToViewController.h"
#import "PhyloAboutInfoViewController.h"
#import "PhyloFAQViewController.h"

@interface PhyloHelpViewController ()

@end

@implementation PhyloHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (IBAction)Phylophynder:(id)sender{ //go to the PhyloPhynder help page
    PhyloInformationViewController *second =[[PhyloInformationViewController alloc]initWithNibName:nil bundle: nil]; //prepare the PhyloPhynder help view
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    second.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self presentViewController:second animated:YES completion:NULL]; //display the PhyloPhynder help view
}
- (IBAction)HowToPlay:(id)sender{ //go to the How To Play help page
    PhyloHowToViewController *second =[[PhyloHowToViewController alloc] initWithNibName:nil bundle: nil]; //prepare the How To Play help view
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    second.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self presentViewController:second animated:YES completion:NULL]; //display the How To Play help view
}


- (IBAction)Credit:(id)sender{ //go to the credits page
    PhyloAboutInfoViewController *second =[[PhyloAboutInfoViewController alloc] initWithNibName:nil bundle: nil]; //prepare the credits page for viewing
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    second.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self presentViewController:second animated:YES completion:NULL]; //switch to the credits page
}










- (void)viewDidLoad
{
    
    UIImage *backgroundImage = [UIImage imageNamed:@"background2.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    [super viewDidLoad];
    [self changeBackButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)faqButtonPressed:(UIButton*)sender {
    PhyloFAQViewController* faqViewCon = [[PhyloFAQViewController alloc]initWithNibName:nil bundle:nil];
    
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    faqViewCon.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self presentViewController:faqViewCon animated:YES completion:nil];
    
}

@end





