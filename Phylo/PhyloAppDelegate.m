/*
//  PhyloAppDelegate.m
//  Phylo
//
//  This file configures the core of PhyloPhynder. It initializes the tab controller and prompts a authentication view. While it handles the overall application process, everything else is done through their individual View-Controller set. 
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//	

*/ 

//importing required classes for tabs
#import "PhyloAppDelegate.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloPlayViewController.h"
#import "PhyloSettingsViewController.h"
#import "PhyloMyStatsViewController.h"




@implementation PhyloAppDelegate
@synthesize theAppDataContainer;
@synthesize viewsArray;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions //the method that gets called when the application starts
{
    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Initialize tabBarController
    self.tabBarController = [[UITabBarController alloc] init];

    // Initialize all the views that will be used in the tabBarController
    UIViewController *viewController2 = [[PhyloPlayViewController alloc] initWithNibName:@"PhyloPlayViewController" bundle:nil];
    UIViewController *viewController3 = [[PhyloSettingsViewController alloc] initWithNibName:@"PhyloSettingsViewController" bundle:nil];
    UIViewController *viewController4 = [[PhyloMyStatsViewController alloc] initWithNibName:@"PhyloMyStatsViewController" bundle:nil];

    
    // Initialize all the navigation controllers needed for display
    UINavigationController *navPlay = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController *navSettings = [[UINavigationController alloc] initWithRootViewController:viewController3];
    UINavigationController *navMyStats = [[UINavigationController alloc] initWithRootViewController:viewController4];

    //TabBar Image
    UIImage* tabBarBackground = [UIImage imageNamed:@"Woodsy.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"selectedWoodsy.png"]];
    
    //Put the views in tabBarController in a viewarray
   viewsArray = [NSArray arrayWithObjects:navPlay,  navMyStats,navSettings,  nil];
    //the following changes all navBar background images
    for (UINavigationController *navCon in viewsArray){
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"WoodsyNavigationBar.png"] forBarMetrics:UIBarMetricsDefault];
        
    }
    
    //set tabbar tabs to viewarray's viewcons
    [self.tabBarController setViewControllers:viewsArray];
    
    //remove tabbar titles
    for (UITabBarItem *item in self.tabBarController.tabBar.items){ 
        [item setTitle:@""];
    }
  
    //Set root controller of the application to tabController
    self.window.rootViewController = self.tabBarController;
    
    //Makes window visible
    [self.window makeKeyAndVisible];
    
    //Initialize a prompt authentication view then prompt it.
    PhyloLoginViewController* logVC = [[PhyloLoginViewController alloc]initWithNibName:@"PhyloLoginViewController" bundle:nil];
    [self.window.rootViewController presentViewController:logVC animated:NO completion:nil];
    
    return YES;
}


//The 

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state. 
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (id) init{
    self.theAppDataContainer = [[PhyloDataContainer alloc]init];
    return [super init];
    
}




@end
