//
//  AppDataContainer.h
//  Phylo
//
// An interface class for PhyloAppDelegateProtocal and PhyloDataContainer
//
//  Created by Jia Jiu Chao on 2013-07-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface AppDataContainer : NSObject

@end
