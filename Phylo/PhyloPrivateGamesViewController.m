//
//  PhyloPrivateGamesViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPrivateGamesViewController.h"
#import "PhyloGames.h"
#import "PhyloQuiz.h"
#import "PhyloCheckpoints.h"
#import "PhyloCard.h"
#import "PhyloMapViewController.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloDataContainer.h"
//#import <CoreLocation/CoreLocation.h>

@interface PhyloPrivateGamesViewController ()
{
    NSMutableArray* data;
    NSMutableArray* searchResults;
    BOOL isFiltered;
    PhyloGames* newPrivateGame;
    UIAlertView* inputPasswordAlert;
    
    NSString* gamePassword;
}

@end

@implementation PhyloPrivateGamesViewController
@synthesize search,dataTable;

- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Private Games", @"Private Games"); //the text under the tab
        self.tabBarItem.image = [UIImage imageNamed:@"first"]; //the logo of the tab
    }
    return self;
}	

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.search.delegate = self;
    self.dataTable.delegate = self;
    self.dataTable.dataSource = self;
    
    
    [self changeBackButton];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getGamesInfo.php?type=private"];
    
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",strResult);
    
    NSArray* GamesStringArr = [strResult componentsSeparatedByString:@"<br>"];
    
    data = [[NSMutableArray alloc]init];
    
    for (int i = 0;i<[GamesStringArr count]-1;i++){
        NSArray* gameInfoArr = [[GamesStringArr objectAtIndex:i]componentsSeparatedByString:@":"];
        NSArray* temparr = [[gameInfoArr objectAtIndex:3] componentsSeparatedByString:@","];
        NSMutableArray* chkpointArr = [[NSMutableArray alloc]initWithArray:temparr];
        PhyloGames* tempGame = [[PhyloGames alloc]initNewGameWithPassword:[gameInfoArr objectAtIndex:0] withCheckpoints:chkpointArr withPassword:[gameInfoArr objectAtIndex:1]];
        
        [data addObject:tempGame];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isFiltered)
        return [searchResults count];
    return [data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell* tableCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(tableCell == nil){
        tableCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    PhyloGames* temp = [[PhyloGames alloc]init];
    if (!isFiltered){
        temp = [data objectAtIndex:indexPath.row];
        tableCell.textLabel.text = temp.gameName;
    }
    else{ //Searching for particular string
        temp= [searchResults objectAtIndex:indexPath.row];
        tableCell.textLabel.text= temp.gameName;
    }
    [tableCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return tableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //open an alert with an OK and cancel button
    
    
    newPrivateGame = [data objectAtIndex:indexPath.row];
    gamePassword = newPrivateGame.password;
    
    
        inputPasswordAlert = [[UIAlertView alloc]initWithTitle:@"Enter the game password." message:@"To join this game, you need to enter the game password below" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
        inputPasswordAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        [inputPasswordAlert textFieldAtIndex:0].delegate = self;
        [inputPasswordAlert show];

    
        
    
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonindex{
    
    //nothing will be done when buttonindex==0, which is cancel
    if (buttonindex == 1){
        //save user data to server... then reset device's profile to nil.
        
        if ([gamePassword isEqualToString:[inputPasswordAlert textFieldAtIndex:0].text]){
            //password correct
            
            NSString *chkpointStr = [NSString stringWithFormat:@"chkpoint[]=%d",[[newPrivateGame.setOfCheckpoints objectAtIndex:0]intValue]];
            
            for (int i=1;i<[newPrivateGame.setOfCheckpoints count];i++){
                chkpointStr =[NSString stringWithFormat:@"%@&chkpoint[]=%d",chkpointStr,[[newPrivateGame.setOfCheckpoints objectAtIndex:i]intValue]];
            }
            
            NSLog(@"%@",chkpointStr);
            
            NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getCheckpointsInfo.php?%@",chkpointStr];
            
            NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
            
            NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
            
            NSArray* chkPointArrUnparsed = [strResult componentsSeparatedByString:@"<br>"];
            
            NSMutableArray* newChkpointArr = [[NSMutableArray alloc]init];
            
            for (int i=0;i<[chkPointArrUnparsed count]-1;i++){
                
                
                
                NSArray* chkPointInfoArr = [[chkPointArrUnparsed objectAtIndex:i]componentsSeparatedByString:@"|"];
                CLLocationCoordinate2D tempCoor;
                tempCoor.latitude = (double) [chkPointInfoArr[2] floatValue];
                tempCoor.longitude = (double) [chkPointInfoArr[3] floatValue];
                NSArray* quizQs = [[NSArray alloc]initWithObjects:chkPointInfoArr[6],chkPointInfoArr[7],chkPointInfoArr[8],chkPointInfoArr[9], nil];
                
                strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/getCardFromID.php?cardID=%d",[chkPointInfoArr[4] intValue]];
                NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
                NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
                NSArray* cardInfo = [strResult componentsSeparatedByString:@"@"];
                
                NSURL *url = [NSURL URLWithString:cardInfo[3]];
                NSData *parsed = [NSData dataWithContentsOfURL:url];
                UIImage *img = [[UIImage alloc] initWithData:parsed] ;
                
                PhyloCard* tempCard = [[PhyloCard alloc]init:cardInfo[0] withWebPath:cardInfo[1] withCardID:[cardInfo[2] intValue] withImage:img];
                
                PhyloQuiz* tempQuiz = [[PhyloQuiz alloc]init:chkPointInfoArr[5] withAnswers:quizQs awardingpoints:[chkPointInfoArr[12]intValue] correctAnswerID:[chkPointInfoArr[10]intValue] withImage:img];
                
                PhyloCheckpoints* tempChkpoint = [[PhyloCheckpoints alloc]initNewCheckpoint:tempCoor withName:chkPointInfoArr[1] withQuiz:tempQuiz withCard:tempCard];
                
                
                
                [newChkpointArr addObject:tempChkpoint];
            }
            
            newPrivateGame.setOfCheckpoints = newChkpointArr;
            
            [self theAppDataContainer].tempGame = newPrivateGame;
            
            PhyloMapViewController *mapViewController = [[PhyloMapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
            [self.navigationController pushViewController:mapViewController animated:YES];
            
        }
        else{
            
            UIAlertView* popUp = [[UIAlertView alloc]initWithTitle:@"Incorrect Password" message:@"Your password was incorrect" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [popUp show];
        }
        
        
    }
}

-(void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope{
    [searchResults removeAllObjects];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF.gameName contains[c] %@",searchText];
    searchResults = [NSMutableArray arrayWithArray:[data filteredArrayUsingPredicate:predicate]];
}

//Search Delegates



//SearchDisplayController Delegates
-(BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:searchOption]];
    return YES;
}

//Function that filters the table view content.
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length==0)
        isFiltered = NO;
    else{
        isFiltered = YES;
        searchResults = [[NSMutableArray alloc]init];
        for (PhyloGames *games in data){
            NSRange stringRange = [games.gameName rangeOfString:searchText options:NSCaseInsensitiveSearch ];
            if (stringRange.location!= NSNotFound)
                [searchResults addObject:games];
            
        }
    }
    [self.dataTable reloadData];
    
}


//Reset the searchBar's input.
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@"";
    
    searchResults = [data mutableCopy];
    [self.dataTable reloadData];
    
    [searchBar resignFirstResponder];
    
}

-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
