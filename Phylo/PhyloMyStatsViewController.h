//
//  PhyloMyStatsViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCardCollectionViewController.h"


@interface PhyloMyStatsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView* statsTable;
    IBOutlet	UIImageView	*imageView;
//    IBOutlet UIButton* cardCollection;
}

@property (nonatomic, strong) UITableView* statsTable;
@property (nonatomic, strong) NSArray *cards;
//@property (nonatomic, retain) UIButton* cardCollection;
-(IBAction)cardCollectionPressed:(id)sender;
-(IBAction)refreshButtonPressed:(id)sender;
-(IBAction)searchButtonPressed:(id)sender;
@end
