//
//  PhyloCardAttributes.h
//  Phylo
/*
    The following class implements the data structure that will be used for PhyloCardViewController. It contains the name, backgroundURL, artURL, hierachyURL, and the size URL. It also contains the most basic initialization method.
 */
//  Created by Jia Jiu Chao on 2013-07-24.
//  Copyright (c) 2013 Terrabyte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloCardAttributes : NSObject

@property (strong,nonatomic) NSString* name;
@property (strong,nonatomic) NSString* backgroundURL;
@property (strong,nonatomic) NSString* artURL;
@property (strong,nonatomic) NSString* hierachyURL;
@property (strong,nonatomic) NSString* sizeURL;

-(PhyloCardAttributes*) initWithCardname:(NSString*)cardname andBackGroundurl:(NSString*)BGurl andArturl:(NSString*)ATRurl andSizeurl:(NSString*)SIZEurl andHierachyurl:(NSString*)HIERACHYurl;

@end
