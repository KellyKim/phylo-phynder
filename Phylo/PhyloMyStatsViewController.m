//
//  PhyloMyStatsViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloMyStatsViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloSearchPageViewController.h"

@interface PhyloMyStatsViewController ()

@end

@implementation PhyloMyStatsViewController
@synthesize statsTable;
@synthesize cards;

//data container for each different login
- (PhyloDataContainer*) theAppDataContainer{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"My Stats", @"My Stats"); //the text under the tab
    }
    return self;
}

- (void)viewDidLoad
{
    //load background image
    UIImageView* backgroundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LogInBackground.png"]];
    [backgroundImage setFrame:self.view.frame];
    
    //load stats table
    self.statsTable.backgroundView = backgroundImage;
    
    PhyloDataContainer* userDataContainer = [self theAppDataContainer];
    NSLog(@"username should be passed here...-> %@",userDataContainer.userName);

    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)searchButtonPressed:(id)sender{ //the search button (that only appears when the user's profile is public) is pressed
    NSLog(@"Search Button Was Pressed");
    
    
    PhyloSearchPageViewController *searchPageViewController = [[PhyloSearchPageViewController alloc] initWithNibName:@"PhyloSearchPageViewController" bundle:nil]; //prepare the profile search page
    [self.navigationController pushViewController:searchPageViewController animated:YES]; //display the profile search page
}

-(void)viewWillAppear:(BOOL)animated{
    

    
    if([self theAppDataContainer].privateOrPublic == 1) //if user's profile is public, he can look at other user's profile
    {
        UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [searchButton setTitle:@"Search" forState:UIControlStateNormal]; //set the title of the button
        [searchButton setTitleColor:[UIColor brownColor] forState:UIControlStateNormal]; //set the color of the title
        searchButton.frame = CGRectMake(240, 15, 70, 25); //set the position and size of the button
        [searchButton setBackgroundImage:[UIImage imageNamed:@"logInbutton.png"] forState:UIControlStateNormal]; //set the image of the button for consistent UI
        searchButton.tag = 111; //this tells us that all searchButtons generated will have the tag 111.  This will come in handy later on, if the user logs off and then logs back on with another public profile, generating another searchButton.  Our solution is to use a for() loop as indicated in the following else{} statement that deletes all buttons with the unique tag 111.
        [searchButton addTarget:self action:@selector(searchButtonPressed:) forControlEvents:UIControlEventTouchUpInside]; //if searchButton is pressed, it will activate the searchButtonPressed function, which displays the Search Page
        [self.view addSubview:searchButton]; //displays the searchButton
    }
    else//otherwise user cant see other user's profile beucase it is set to private
    {
        for(UIButton *button in [self.view subviews])
        {
         [[self.view viewWithTag:111] removeFromSuperview]; //deletes all buttons in this view with unique tag 111
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { //number of sections in table
    return 1; //we only need one section.
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName;
    switch(section){
        case 0://name of the first table section. we only have one section for now, so other values will default to nothing.
            sectionName = NSLocalizedString(@"My profile statistics", @"My profile statistics");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    //set up the cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0){ //there is, without a doubt, a more efficient way to implement row items.
        cell.textLabel.text = [NSString stringWithFormat:@"Cards collected:"]; //text to the left of the cell is "Cards collected:"
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [self theAppDataContainer].cardsCollected]; //text to the right of the cell is the amount of cards the user has in his/her digital collection
    }
    else if ([indexPath row] == 1){
        cell.textLabel.text = [NSString stringWithFormat:@"Total points:"]; //left cell text is "Total points:"
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d",[self theAppDataContainer].points]; //right cell text is how many points the current user has
    }
    else if ([indexPath row] == 2){
        cell.textLabel.text = [NSString stringWithFormat:@"Total number of games played:"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d",[self theAppDataContainer].gamesCompleted]; //right cell text is the number of games the user has completed
    }
    else if ([indexPath row] == 3){
        cell.textLabel.text = [NSString stringWithFormat:@"Member since:"];
        cell.detailTextLabel.text = [self theAppDataContainer].dateJoined; //right cell text is the date the account was created in the PhyloPhynder database.
    }
    
    
    return cell;
}

-(IBAction)cardCollectionPressed:(id)sender{
    //prepares the card collection screen
    PhyloCardCollectionViewController *cardCollectionViewController = [[PhyloCardCollectionViewController alloc] initWithNibName:@"PhyloCardCollectionViewController" bundle:nil];
    //adds the card collection screen to the top of the view stack and displays it
    [self.navigationController pushViewController:cardCollectionViewController animated:YES];
}

-(IBAction)refreshButtonPressed:(id)sender{
    //refreshes the data of the table
    [self.statsTable reloadData];
}

//JACKY: I honestly don't think we need a refresh button. We should let the view handle updates when it is about to appear.
-(void)viewDidAppear:(BOOL)animated{
    [self.statsTable reloadData];
}

@end
