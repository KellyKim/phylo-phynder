//
//  PhyloCardDetailsViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCardDetailsViewController.h"

@interface PhyloCardDetailsViewController ()

@end

@implementation PhyloCardDetailsViewController
@synthesize cardWebView;
//URLGoTo is always changed by the button that calls it, as seen in PhyloCardCollectionViewController.m
@synthesize URLGoTo;
@synthesize detailsTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //custom
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //sets the title of the current page to the card as selected from the previous page
    self.title = detailsTitle;
    
    //loads the correspondig card page of the button that we clicked
    [cardWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URLGoTo]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end