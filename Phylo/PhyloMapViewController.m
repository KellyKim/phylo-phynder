//
//  MapViewController.m
//  Phylo
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloAppDelegate.h"
#import "PhyloMapViewController.h"
#import "PhyloMapViewAnnotation.h"
#import "PhyloQuizViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloExtraFunctions.h"

@implementation PhyloMapViewController
@synthesize mapView,tempCheckpoint,setOfCheckpoints,setOfAnnotations;

//---------------------
//Update Container
//---------------------
- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}



//---------------------------------------------
// CORE LOCATION METHOD
//---------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Create location manager object
        locationManager = [[CLLocationManager alloc] init];
        
        // There will be a warning from this line of code; ignore it for now
        [locationManager setDelegate:self];
        
        // And we want it to be as accurate as possible
        // regardless of how much time/power it takes
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        

    }
    
    return self;
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Could not find location: %@", error);
}

- (void)mapView:(MKMapView *)thisMapView
didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    CLLocationCoordinate2D loc = [userLocation coordinate];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 500, 500);
    [thisMapView setRegion:region animated:YES];
    
}



// Received memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


// Deallocations


CLLocationManager *_locationManager;
NSArray *_regionArray;

- (void)viewDidLoad
{
    //show user location
    [mapView setShowsUserLocation:YES];
    
    [super viewDidLoad];
    
    [self initializeLocationManager];
    
    
    //setup map's checkpoints and cooresponding annotation for checkpoints received
    self.setOfCheckpoints = [[NSMutableArray alloc]initWithArray:[self theAppDataContainer].tempGame.setOfCheckpoints];
    self.setOfAnnotations = [[NSMutableArray alloc]init];
    
    for (int i=0;i<[setOfCheckpoints count];i++){
        PhyloCheckpoints* chkpointAti = [self.setOfCheckpoints objectAtIndex:i];
            [self.setOfAnnotations addObject:[[PhyloMapViewAnnotation alloc] initWithTitle:chkpointAti.checkpointTitle  andCoordinate:chkpointAti.locationCoordinate]];
            [self.mapView addAnnotation:[self.setOfAnnotations objectAtIndex:i]];
        
        
    }

    //changes the back button's image
    [self changeBackButton];


}

- (void)viewDidUnload
{
   
    [self setMapView:nil];
    [super viewDidUnload];
    [super viewDidUnload];
	mapView = nil;
}
 
- (void)viewWillAppear:(BOOL)animated{
    //let gameComplete = true regardless. It all checkpoints in the current game are visited, leave it at True. Else, make it false.
    BOOL gameCompleted = YES;
    for (PhyloCheckpoints *chkpointi in setOfCheckpoints){
        if(chkpointi.quiz.quizCompleted == NO)
            {
            gameCompleted = NO;
            break;}
    }
    
    //If gamecomplete is set to True after the previous validation routine, followup by setting the datacontainer's gamecomplete to True. This is used to inform a pop up alert at the rootviewcon. Finally, pop to rootviewcon. 
    if (gameCompleted == YES){
        [self theAppDataContainer].tempGame.completed = YES;
        
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
        
    
}


// This is the orientation method that will not be used for this project.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
//---------------------------------------------------
// Initialize location manager
//---------------------------------------------------

- (void)initializeLocationManager {
    // Check to ensure location services are enabled
    if(![CLLocationManager locationServicesEnabled]) {
        [self showAlertWithMessage:@"You need to enable location services to use this app."];
        return;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
}

//--------------------------------------------------
//alert message
//--------------------------------------------------
- (void)showAlertWithMessage:(NSString*)alertText {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services"
                                                        message:alertText
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

//--------------------------------------------------
//Found it button Checks if you are at a checkpoint
//--------------------------------------------------
-(IBAction)foundIt:(id)sender{
    
    
    //calculate user location
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:mapView.userLocation.location.coordinate.latitude longitude:mapView.userLocation.location.coordinate.longitude];
    
    /*------------------------------------------------------
     find the nearest point according to user location; This is done by first setting a extremely large distance to distanceClosest. Then, find the nearest point to the user, calculate that distance, and then set that value to the distanceClosest variable.
     -------------------------------------------------------*/
    CLLocationDistance distanceClosest=999;
    NSInteger closestIndex = 0;
    for (int i=0;i<[setOfCheckpoints count];i++){
        CLLocationDistance distFromi = [[[setOfCheckpoints objectAtIndex:i] locationFormat] distanceFromLocation:locA];
        if (distFromi< distanceClosest){
            distanceClosest = distFromi;
            closestIndex = i;
        }
    }
    
    

    /*------------------------------------------------------
     The following routine does as the following.
     First, make sure the closest point is no further than 40 meters from user. If it is, remove the closest checkpoint i from the datacontainer's tempcheckpoints, and show the quiz it contains. Also remove the corresponding checkpoint and annotation in this viewcon. Else, alert the user a message. This alert will also show when the quiz was previously completed.
    -------------------------------------------------------*/
    PhyloDataContainer* updateTempData = [self theAppDataContainer];
    if(distanceClosest <= 40){
        updateTempData.tempCheckpoints = [setOfCheckpoints objectAtIndex:closestIndex];
        updateTempData.tempQuiz = updateTempData.tempCheckpoints.quiz;
        if (updateTempData.tempQuiz.quizCompleted == YES)
            [self showAlertWithMessage:@"You are not at any checkpoint"];
        else{
            //display the quiz
            [self showAlertWithMessage:[NSString stringWithFormat:@"You are at %@.", updateTempData.tempCheckpoints.checkpointTitle]];
            [self presentViewController:[[PhyloQuizViewController alloc]init] animated:YES completion:nil];
            [self.mapView removeAnnotation:[setOfAnnotations objectAtIndex:closestIndex]];}
    }else{
    
        [self showAlertWithMessage:@"You are not at any checkpoint"];
        
    }

}

// a method to change the back button's image.
-(void)changeBackButton{
    UIImage *image= [UIImage imageNamed:@"WoodsyBack.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, image.size.width,image.size.height);
    [button addTarget:self action:@selector(popit) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [buttonView addSubview:button];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:buttonView];
    
    self.navigationItem.leftBarButtonItem = backItem;
}
// used by changeBackButton as a action selector
- (void) popit{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
