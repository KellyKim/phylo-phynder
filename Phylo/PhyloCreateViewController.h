//
//  PhyloCreateViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhyloSelectMapViewController.h"

@interface PhyloCreateViewController : UIViewController <UITextFieldDelegate> {
    UISegmentedControl *privatePublic; //the option for a private or public game
    UITextField *password; //password for a private game
    UITextField *nameOfScavengerHunt; //name of the scavenger hunt
    NSString *string;
}

@property (nonatomic, strong) IBOutlet UISegmentedControl *privatePublic;
@property (nonatomic,strong) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *nameOfScavengerHunt;
@property (copy, nonatomic) NSString *string;

-(IBAction) dismissKeyboard:(id)sender;
-(IBAction) privatePublicIndexChanged;
-(IBAction) nextButtonPressed:(id)sender;

@end
