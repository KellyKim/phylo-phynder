//
//  PhyloCardCollectionViewController.h
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCardDetailsViewController.h"

@interface PhyloCardCollectionViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *cardURLs;
@property (nonatomic, strong) NSMutableArray *cardName;
@property (nonatomic, strong) NSMutableArray *cardImage;
@property (weak, nonatomic) IBOutlet UITableView *cardTable;



@end
