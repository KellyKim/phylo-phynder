//
//  PhyloLoginViewController.m
//  Phylo
//
//  Created by Jacky Chao on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloLoginViewController.h"
#import "PhyloSignUpViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegate.h"
#import "PhyloAppDelegateProtocol.h"

@interface PhyloLoginViewController (){
    

}

@end

@implementation PhyloLoginViewController


- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //setup path and filename to store and read from userinfo
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"LogInBackground.png"]]];
    //Bool fileExist = [[NSFileManager]]
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //reset animation type
    self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
 }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#define username @"guest"
#define password @"abc123"

//events to go through when login button is pressed
- (IBAction)Login:(UIButton *)sender { //Authentication function
    
    //empty UserID or password
    if([_nameField.text isEqualToString:@""] || [_passwordField.text isEqualToString:@""])
    {
        //alert user
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Fill in all fields." delegate: self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    //database connection
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/login2.php?userName=%@&password=%@",
                        _nameField.text, _passwordField.text];
    
    //retrieve data from data base
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    //convert into string
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
   
    NSLog(@"%@",strResult);
    
    //pop up message
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged in." message:@"Congratuations! You are now logged in." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    // if only one row is returned from the database, authentication is confirmed
    // 2013/07/05: database is gone so I prepared a local account for testing
    
    //commented the following line out because the database is working
    //if(![strResult isEqualToString:@"0"]||!([_nameField.text isEqualToString:@"guest"]&&[_passwordField.text isEqualToString:@"abc123"]))
    
    //if there at least 1 matching result and the result is not empty, then login success
    if(![strResult isEqualToString:@"0"]&&![strResult isEqualToString:@",,,,,"])
    {
        NSArray *dataArr = [strResult componentsSeparatedByString:@","];
        
        //override all data assosicated with the new user. The different indexes of dataArr[] indicated in the following lines contain the data returned by the .php script, which allows us to get the required data of the user.
        
        PhyloDataContainer* updateDataContainer = [self theAppDataContainer];
        updateDataContainer.userName = dataArr[0];
        updateDataContainer.dateJoined = dataArr[1];
        
        NSArray *amountOfCardsCollected = [dataArr[2] componentsSeparatedByString:@"!"];
        NSLog(@"amountofCardscollected: %@", amountOfCardsCollected);
        if([amountOfCardsCollected[0] isEqualToString:@""])
        {
            updateDataContainer.cardsCollected = 0;
        }
        else
        {
            updateDataContainer.cardsCollected = [amountOfCardsCollected count];
        }
        
        updateDataContainer.gamesCompleted = [dataArr[3] intValue];
        updateDataContainer.points = [dataArr[4] intValue];
        updateDataContainer.privateOrPublic = [dataArr[5] intValue];
        updateDataContainer.userPassword = _passwordField.text;
        updateDataContainer.soundEnabled = YES;
        NSLog(@"%d",updateDataContainer.privateOrPublic);
        
        //update other things in viewWillAppear of StatsViewCon
        
        //set tabbar controller's view to first one
        PhyloAppDelegate* delegate = (PhyloAppDelegate*)[[UIApplication sharedApplication]delegate];
        delegate.tabBarController.selectedIndex = 0;
        [[delegate.viewsArray objectAtIndex:0] popToRootViewControllerAnimated:YES];
        [[delegate.viewsArray objectAtIndex:1] popToRootViewControllerAnimated:YES];
        [[delegate.viewsArray objectAtIndex:2] popToRootViewControllerAnimated:YES];
        
            [self dismissViewControllerAnimated:YES completion:nil]; //dismiss
            
            [alert show];
    }else
    {//inccorect login info
                alert.title = @"Oops!";
                alert.message = @"Incorrect UserName/Password. Please try again.";
                [alert show];
    }
    

    
}

//sign up button
- (IBAction)newUser:(UIButton *)sender { //Registers a new user.
    PhyloSignUpViewController *signup = [[PhyloSignUpViewController alloc] initWithNibName:nil bundle:NULL];
    [self presentViewController:signup animated:YES completion:nil];
}




//dismiss keyboard when background is clicked
- (IBAction)backgroundClick:(id)sender{
    [self.nameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
}


-(BOOL) textFieldShouldReturn:(UITextField*)textField{
    textField.delegate = self;
    NSLog(@"Clicked");
    [self Login:nil];
    return YES;
    
}




@end
