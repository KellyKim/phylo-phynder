//
//  PhyloGames.m
//  Phylo
//
//  Created by Lydia Huang on 2013-07-01.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloGames.h"

@implementation PhyloGames
@synthesize gameName;
@synthesize  setOfCheckpoints;
@synthesize password;
@synthesize passwordProtected;
@synthesize completed;

//create public game with a name, array of check points
- (PhyloGames* )initNewGame:(NSString*)name withCheckpoints:(NSMutableArray*) providedCheckpoints{
    self = [super init];
    
    if (self) {
        //initializing variables
        gameName = name;
        setOfCheckpoints = providedCheckpoints;
        password = nil;
        passwordProtected = NO;
        
    }
    return self;
    
}

//create a private game with password
- (PhyloGames* )initNewGameWithPassword: (NSString*)name withCheckpoints:(NSMutableArray*) providedCheckpoints withPassword:(NSString*)providedPassword{
    self = [super init];
    
    if (self) {
        //initializing variables
        gameName = name;
        setOfCheckpoints = providedCheckpoints;
        password = providedPassword;
        passwordProtected = YES;
    }
    
    return self;
    
}



@end
