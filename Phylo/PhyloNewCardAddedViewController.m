//
//  PhyloNewCardAddedViewController.m
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloNewCardAddedViewController.h"

@interface PhyloNewCardAddedViewController ()

@end

@implementation PhyloNewCardAddedViewController

@synthesize imageView, theImage;
@synthesize nameOfCardAdded;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //display an alert to the user saying that the user just added X to their card database, where X is the card they just discovered at a checkpoint.
    UITextView *cardAddedText = [[UITextView alloc] initWithFrame:CGRectMake(10,20,300,50)];
    [cardAddedText setBackgroundColor:[UIColor clearColor]];
    [cardAddedText setText:[NSString stringWithFormat:@"Congratulations! You just added %@ to your database!",nameOfCardAdded]];
    [cardAddedText setFont:[UIFont systemFontOfSize:16]];

    [[self view] addSubview:cardAddedText];
    
    //display on the background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sunburstBackground.png"]];
    [imageView setImage:theImage];

}

-(IBAction)okayButtonPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
