//
//  PhylohelpHowToViewController.h
//  Phylo
//
//  Created by Michael Riyanto on 2013-07-14.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloHowToViewController : UIViewController{
    IBOutlet UIScrollView *scroller;
}

- (IBAction)back:(id)sender;

@end
