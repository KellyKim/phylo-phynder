//
//  MapViewAnnotaion.h
//  Phylo
//
// This class implements a single annotation that can be placed in PhyloMapViewController. It contains an annotation title and a coordinate struct. It connects with PhyloCheckpoints to create the marker on the map. 
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PhyloMapViewAnnotation : NSObject <MKAnnotation> {
    
	NSString *title;
	CLLocationCoordinate2D coordinate;
    
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d;

@end
