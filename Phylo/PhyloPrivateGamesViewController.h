//
//  PhyloPrivateGamesViewController.h
//  Phylo
/*----------------------------------------------
 The class is identical to PublicGamesViewCon with the extra password insertion alert. It pulls from a different table on the database. 
 ----------------------------------------------*/
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloPrivateGamesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIAlertViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *search;
@property (weak, nonatomic) IBOutlet UITableView *dataTable;

@end
