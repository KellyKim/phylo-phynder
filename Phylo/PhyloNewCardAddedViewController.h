//
//  PhyloNewCardAddedViewController.h
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloNewCardAddedViewController : UIViewController
{
    UIImageView *imageView;
    UIImage *theImage;
}
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) UIImage *theImage;

@property (nonatomic, retain) NSString *nameOfCardAdded;

-(IBAction)okayButtonPressed:(id)sender;
//-(void)setImage:(UIImage *)image;

@end
