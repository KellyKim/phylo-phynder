//
//  PhyloFAQViewController.m
//  Phylo
//
//  Created by Jia Jiu Chao on 2013-07-27.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloFAQViewController.h"

@interface PhyloFAQViewController ()

@end

@implementation PhyloFAQViewController
@synthesize textview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    textview.text = @"How do I create a game?\nA:You can create a game at our server page:http://www.phylophynder.evopoint.net/creategame.php\n\nOnce Im in a hunt can I get out of it?\nA:Yes,to get out from the hunt just close the application. You will be resign automatically from the game.\n\nHow can I get in touch with the creator of this application?\nA:Please use the contact page on our website:http://www.i-m.co/misskimkim/cmpt275Group15/\n\nI found a bug and I have an awesome feature request, how can I tell you about his?\nA:Same as above, please use the contact information on our website:http://www.i-m.co/misskimkim/cmpt275Group15/. We'd love to hear from you.\n\nHow can I add friends to my existing hunt?\nA:Your friends need to log in into the apps first and need to use join existing hunt button.\n\nHow come I'm not getting any sound? \nA:Make sure you are not in silent mode and your sound settings are turned on.";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonClicked:(UIButton*)sender{

    //clicking anywhere allows to go back to previous screen
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    self.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
