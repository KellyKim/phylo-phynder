//
//  PhyloQuiz.h
//  Phylo
//
//  This class implements a quiz object that will be attached to every checkpoint object. It contains a question, an array of answer strings, points, the correct answer ID, a display image (card's), and a boolean that determines if the quiz is completed. 
//
//  Created by Jacky Chao on 2013-07-01.
//  Copyright (c) 2013 TerraByte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloQuiz : NSObject{
}

@property (nonatomic,strong) NSString* question;
@property (nonatomic,strong) NSArray* answerSet; 
@property (nonatomic,assign) int points;
@property (nonatomic,assign) int correctAnswerID;
@property (nonatomic,strong) UIImage* displayImage;
@property (nonatomic,assign) BOOL quizCompleted;

-(PhyloQuiz*) init:(NSString*)quizQuestion withAnswers:(NSArray*)providedAnswerSet awardingpoints:(int)quizPoints correctAnswerID:(int)ID withImage:(UIImage*)quizImage;
-(BOOL) quizCompletion:(NSInteger)selectedAnswerID;
-(NSMutableArray*) randomizeAnswerSet;
-(void) updateToDatabase;



@end
