//
//  PhyloAccountSettingsViewController.h
//  Phylo
//
//  Created by Cody Santos on 7/19/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloAccountSettingsViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate>

- (IBAction)deleteAccountPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *soundLabel;
@property (weak, nonatomic) IBOutlet UISwitch *soundEnableSwitch;
- (IBAction)switchChanged:(UISwitch*)sender;

@end
