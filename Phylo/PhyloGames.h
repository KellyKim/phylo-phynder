//
//  PhyloGames.h
//  Phylo
//
//  Created by Lydia Huang on 2013-07-01.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloGames : NSObject{

}

@property (nonatomic,strong) NSString* gameName;
@property (nonatomic,strong) NSMutableArray* setOfCheckpoints;
@property (nonatomic,strong) NSString* password;
@property (nonatomic,assign) BOOL passwordProtected;
@property (nonatomic,assign) BOOL completed;


// Game Constructor
- (PhyloGames* )initNewGame:(NSString*)name withCheckpoints:(NSMutableArray*) providedCheckpoints;
- (PhyloGames* )initNewGameWithPassword: (NSString*)name withCheckpoints:(NSMutableArray*) providedCheckpoints withPassword:(NSString*)providedPassword;

// Game checkpoint checker
//- (BOOL) hasAccessedCheckpoint: (Checkpoint*)checkpoint;





@end
