//
//  Quiz_GameViewController.m
//  Quiz Game
//
//

#import"PhyloQuizViewController.h"
#import "PhyloDataContainer.h"
#import "PhyloAppDelegateProtocol.h"
#import "PhyloNewCardAddedViewController.h"
#import "PhyloExtraFunctions.h"


@implementation PhyloQuizViewController{
    UIAlertView* areYouSure;
    NSInteger userAnswer;
}

@synthesize theQuestion, theScore, answerOne, timer,answerTwo, answerThree, answerFour, theQuiz,extra, quiz;
@synthesize backgroundImage;


- (PhyloDataContainer*) theAppDataContainer;
{
    id<PhyloAppDelegateProtocol> theDelegate = (id<PhyloAppDelegateProtocol>)[UIApplication sharedApplication].delegate;
    PhyloDataContainer *dataContainer;
    dataContainer = (PhyloDataContainer*)theDelegate.theAppDataContainer;
    return dataContainer;
}


-(void)askQuestion
{
    
    // CREATES 4 RANDOM NUMBERS
    //randomnumber = arc4random() % 5;  
    
	// Unhide all the answer buttons.
	[answerOne setHidden:NO];
	[answerTwo setHidden:NO];
	[answerThree setHidden:NO];
	[answerFour setHidden:NO];
	
	// Set the game to a "live" question (for timer purposes)
	questionLive = YES;
	
	// Set the time for the timer
	time = 100.0;
	
NSLog(@"This is question number:%@", self.quiz.question);
    //NSLog(@"This is question number:%d", self.quiz.points);
    //NSLog(@"This is question number:%@", self.quiz.answerSet);
    //NSLog(@"This is question number:%d", self.quiz.correctAnswerID);
    //NSLog(@"This is question number:%@", self.quiz);
    NSLog(@"This is question number:%d", questionNumber);
    

	
	//image fetched from quiz object
    imageView.image = self.quiz.displayImage;

	
	
    // Set the question string, and set the buttons the the answers

	
	[answerOne setTitle:[self.quiz.answerSet objectAtIndex:0] forState:UIControlStateNormal];
	[answerTwo setTitle:[self.quiz.answerSet objectAtIndex:1] forState:UIControlStateNormal];
	[answerThree setTitle:[self.quiz.answerSet objectAtIndex:2] forState:UIControlStateNormal];
	[answerFour setTitle:[self.quiz.answerSet objectAtIndex:3] forState:UIControlStateNormal];
    
	rightAnswer = self.quiz.correctAnswerID;
	
	// Set theQuestion label to the active question
	theQuestion.text = self.quiz.question;
	
	// Start the timer for the countdown
	timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
	

	
    
}





// Check for the answer
-(void)checkAnswer:(int)theAnswerValue
{
    BOOL correct = [self.quiz quizCompletion:theAnswerValue];
	if(correct)
	{
		theQuestion.text = @"Correct!!";
        [self theAppDataContainer].soundEnabled? [PhyloExtraFunctions playSound:@"quiz_correct" withType:@"mp3"] : nil;
        
	}
	else
	{

        NSInteger row;
        if(questionNumber == 1)
        {
           // row = questionNumber - 1;
            row = ((questionNumber - 1) * 6);

        }
        else
        {
            row = ((questionNumber - 1) * 6);
        }
        row = row +rightAnswer;

       // NSString *checkselected = [quiz objectAtIndex:row];

        //NSLog(@"This is question number: %d", questionNumber);
        NSString *checkQuestion = [[NSString alloc] initWithFormat:@"You are wrong, the answer is: %@", [self.quiz.answerSet objectAtIndex:self.quiz.correctAnswerID]];

        
        theQuestion.text = checkQuestion;
        [self theAppDataContainer].soundEnabled? [PhyloExtraFunctions playSound:@"fail" withType:@"mp3"] : nil;
        
    }
	[self updateScore];
}

-(void)updateScore
{
	// If the score is being updated, the question is not live
	questionLive = NO;
	
	[self.timer invalidate];
	
	// Hide the answers from the previous question
	[answerOne setHidden:YES];
	[answerTwo setHidden:YES];
	[answerThree setHidden:YES];
	[answerFour setHidden:YES];

    myScore = [self theAppDataContainer].points;
	NSString *scoreUpdate = [[NSString alloc] initWithFormat:@"Score: %d", myScore];
	theScore.text = scoreUpdate;
	
    //PhyloDataContainer *updateTempData = [self theAppDataContainer];
    
    imageView.image = self.quiz.displayImage;
    
    // the server adds it to database if it's not in our collection already.  we just get the result of whether it is or is not in our collection already.
    NSString *strURL = [NSString stringWithFormat:@"http://phylophynder.evopoint.net/addCardToCollection.php?userName=%@&cardID=%d", [self theAppDataContainer].userName, [self theAppDataContainer].tempCheckpoints.card.cardIdentifier];
    
    NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    
    NSString *strResult = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
    
    NSLog(@"For debugging: %@",strResult);
    
    //if its already in our collection do nothing
    if([strResult isEqualToString:@"AlreadyInCollection"])
    {
        //do nothing
    }
    //if it wasn't in our collection already let the user know that it has been added
    else
    {
        PhyloNewCardAddedViewController *newCardAdded = [[PhyloNewCardAddedViewController alloc] initWithNibName:@"PhyloNewCardAddedViewController" bundle:nil];
    
        //newCardAdded.imageView = imageView;
        newCardAdded.theImage = imageView.image;
        newCardAdded.nameOfCardAdded = [self theAppDataContainer].tempCheckpoints.card.cardName;
        //[self.navigationController pushViewController:newCardAdded animated:YES];
        [self theAppDataContainer].cardsCollected += 1;
        [self presentViewController:newCardAdded animated:YES completion:nil];
    }

	// END THE GAME.
	
		[self.extra setHidden:NO];
		//[self.extra setTitle:@"Let's start our next Adventure!" forState:UIControlStateNormal];

	
}





-(void)countDown
{
	// Question live counter
	if(questionLive==YES)
	{
		time = time - 1;
		if(time == 0)
		{
			questionLive = NO;
			theQuestion.text = @"Time's up! Sorry, but you do not get a card :'( ";
			myScore = myScore - 10;
			[self.timer invalidate];
			[self updateScore];
		}
	}
	// In-between Question counter
	else
	{
		time = time - 1;
	
		if(time == 0)
		{
			[self.timer invalidate];
			[self askQuestion];
		}
	}
	if(time < 0)
	{
		[self.timer invalidate];
	}
}


- (IBAction)buttonOne
{
	if(questionNumber == 0){
		// startup-state
		//start the game.
		[answerTwo setHidden:NO];
		[answerThree setHidden:NO];
		[answerFour setHidden:NO];
		[self askQuestion];
        questionNumber++;
	}
	else
	{
        [areYouSure show];
		userAnswer = 0;
        
	}
  //  NSLog(@"Give me message.");
}

- (IBAction)buttonTwo
{
    [areYouSure show];
	userAnswer = 1;
}

- (IBAction)buttonThree
{
    [areYouSure show];
	userAnswer = 2;
}

- (IBAction)buttonFour
{
    [areYouSure show];
	userAnswer = 3;
	
}

- (IBAction)dismissThisView:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}





-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    [self checkAnswer:(int)userAnswer];
    
}






// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [self theAppDataContainer].soundEnabled? [PhyloExtraFunctions playSound:@"LaOSound" withType:@"mp3"] : nil;
    UIImageView* bGroundImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"backgroundForQUIZ.jpg"]];
    [backgroundImage setFrame:self.view.frame];
    
    
    self.backgroundImage = bGroundImg;
	questionLive = NO;
    
    theQuestion.text = @"Congratulations, You have reached a checkpoint";
   
    PhyloDataContainer* updateContainer = [self theAppDataContainer];
    myScore = updateContainer.points;
	theScore.text = [NSString stringWithFormat:@"Score: %d",myScore];
	questionNumber = 0;
    
    self.quiz = updateContainer.tempQuiz;
    
    areYouSure = [[UIAlertView alloc]initWithTitle:@"Final Answer?" message:@"Warning: Deduction of 10 points if you answer incorrectly!" delegate:self cancelButtonTitle:@"Hold on..." otherButtonTitles:@"Yes", nil];
    
	[answerOne setTitle:@"Click here to start your quiz!" forState:UIControlStateNormal];
	[answerTwo setHidden:YES];
	[answerThree setHidden:YES];
	[answerFour setHidden:YES];
	[self loadQuiz];
}

-(void)loadQuiz
{

    
    
    
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
}




- (void)viewDidUnload {
    
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{

}


@end
