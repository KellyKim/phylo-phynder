//
//  HelpSecondviewViewController.m
//  Phylo
//
//  Created by Michael Riyanto on 7/11/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloInformationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PhyloFAQViewController.h"

@interface PhyloInformationViewController ()

@end

@implementation PhyloInformationViewController
@synthesize textView,logo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    
    [scroller setScrollEnabled:YES];
    
    textView.layer.borderWidth = 1;
    textView.layer.borderColor = [UIColor blackColor].CGColor;
    
    //create the view


    textView.text = @"Phylo-Phynder is a fun way for adolescents to learn actively about animals. It incorporates a scavenger hunt and several short quizes about animals as well as their ecosystems. This application is suitable for a students aged 7-17 who wants to learn more about animals and can be a great education tool.\n\nThis is similar to the idea of “The Amazing Race”. Users can create their own scavenger hunt in their own area for others to play. \n\nGame Play:\nThe app will have two main features including the playable portion and a customizable portion. For the playable portion, users will have to go to each checkpoint displayed on their virtual map and answer a quiz. If they get it right, they earn points and will be rewarded by Phylo card. You can add your own games at our offiicla game creation page:http://www.phylophynder.evopoint.net/creategame.php/\n\nPhylo Card is a collectible card game based on the Phylo project. It is aplayable card game that makes use of the wonderful, complex and inspiring things that inform the notion of biodiversity.User will gets a card when they answer a quiz correctly.For more information please visit http://phylogame.org/ \n\n\n";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender { //back button was pressed
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    self.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self dismissViewControllerAnimated:YES completion:nil]; //dismiss the current view
}

-(void)viewDidAppear:(BOOL)animated
{
    //reset animation type
    self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
}



@end
