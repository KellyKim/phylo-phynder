//
//  PhyloAboutInfoViewController.m
//  Phylo
//
//  Created by Michael Riyanto on 2013-07-26.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloAboutInfoViewController.h"

@interface PhyloAboutInfoViewController ()

@end

@implementation PhyloAboutInfoViewController
@synthesize textView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    //version info of phylophynder and credits
    textView.backgroundColor =[UIColor clearColor];
    
    textView.text = @"PhyloPhynder!\nVersion 1.0\nCopyright(c) TerraByte 2013\n\n\n\nProject manager\nKelly Kim\n\nLead Developer\nCody Santos\n\nLead Designer\nMichael Riyanto\n\nAll-round Assistance\nJacky Chao\n\nDatabase Management\n Yun Zeng\n\n";
}

//back to previous screen
- (IBAction)back:(id)sender {
    UIViewAnimationTransition animation = UIViewAnimationTransitionFlipFromLeft;
    self.modalTransitionStyle = animation;
    [UIView setAnimationTransition:animation forView:	self.view cache:YES];
    [self dismissViewControllerAnimated:YES completion:nil]; //dismiss the current view
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
