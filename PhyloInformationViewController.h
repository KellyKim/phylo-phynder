//
//  HelpSecondviewViewController.h
//  Phylo
//
//  Created by Michael Riyanto on 7/11/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloInformationViewController : UIViewController{
    IBOutlet UIScrollView *scroller;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)back:(id)sender;


@end
